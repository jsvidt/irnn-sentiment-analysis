from rnn import *
from sentiment.loadsentences import *
from kfold import *

words, inputs, outputs = loadsentences(number=1000)
print "Input length: ", len(inputs)
print "Word length: ", len(words)
def createnn():
    rnn = Network(
            input_size=len(words),
            lrate=0.005,
            drate=0.1,
            cost=cross_entropy,
            cost_deriv=log_likelihood_deriv,
            cutoff=10,
            #reg_beta=0.001,
            #reg=l2norm,
            #reg_deriv=l2norm_deriv,
            clip_gradient=1,
            decay_frequency=40,
            decay_rate=0.1,
        )


    rnn.add_layer(Layer(
        size=100,
        #activation=sigmoid,
        #activation_deriv=sigmoid_deriv
        ))
    #rnn.add_layer(Layer(
    #    size=10,
    #    weight_factor=1,
        #activation=sigmoid,
        #activation_deriv=sigmoid_deriv
    #    ))

    #rnn.add_layer(Layer(
    #    size=10,
    #    activation=identity,
    #    activation_deriv=identity_deriv,
    #    ))

    rnn.add_layer(Layer(
        size=2,
        activation=softmax,
        activation_deriv=softmax_deriv,
        ))

    return rnn


def every(nn,outputs,guesses):
    guess_dist = np.zeros(nn.layers[-1].size)
    correct = 0
    #print guesses[:,:,-1]
    for i,guess in enumerate(guesses):
        guess = guess[-1]
        
        #if k % 1000 == 0:
        #    print "guess", guess
        #    print "output", outputs[i]
        #print "max: ",np.argmax(guess)
        
        guess_dist[guess.ravel().argmax()] += 1
        correct += np.argmax(guess) == np.argmax(outputs[i])
        #print reconstruct_sentence(i,words,inputs),
        #print guess[-1]
    print "correct {}/{}".format(correct,len(guesses))
    print "guess dist {}".format(guess_dist)
    print "guess mean {}".format(guesses[0].mean(axis=0))
    print "true dist {}".format(outputs.sum(axis=0))

kFoldCross(createnn,
        "sentence_inputs.npy",
        #"imgs.txt.npy",
        "sentence_outputs.npy",
        #"labels.txt.npy",
        #"eeg_inputs.npy",
        #"eeg_outputs.npy",
        check=every,
        batch=None,
        iterations=1000,
        folds=10,
        super_iterations=1,
        #save_in="mnist_validation",
        save_in="sent_validation",
        #max_inputs=500
        )