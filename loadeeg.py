import numpy as np
from rnn import *

def load_eeg(number=None):
    data = np.loadtxt("EEG Eye State.txt",delimiter=",")
    if number:
        data[:number]
    inputs = []
    outputs = []

    _inputs = data[:,:data.shape[1]-1]
    print _inputs.max(axis=1)
    print _inputs.min(axis=1)
    print _inputs.mean(axis=1)
    print "in",_inputs
    #_inputs /= 1000
    _inputs /= _inputs.max(axis=0)
    #_inputs -= _inputs.mean(axis=0)
    #_inputs += 1
    print _inputs.max(axis=1)
    print _inputs.min(axis=1)
    print _inputs.mean(axis=1)
    
    _outputs = np.zeros((_inputs.shape[0],2))
    labels = data[:,-1]
    _outputs[labels == 0,0] = 1
    _outputs[labels == 1,1] = 1
    
    seq_len = 10
    for i in xrange(0,_inputs.shape[0],seq_len):
        inputs.append(_inputs[i:i+seq_len,:])
        outputs.append(_outputs[i:i+seq_len,:])
        
    
    

    
    #inputs = inputs[:,:,np.newaxis]
    #print inputs.max(axis=0),inputs.mean(axis=0),inputs.min(axis=0)
    
    return np.array(inputs),np.array(outputs)


inputs, outputs = load_eeg(number=14000)
#print inputs.max(axis=1)
#print inputs
if __name__ == "__main__":
    
    rnn = Network(
            input_size=inputs[0].shape[1],
            lrate=0.00001,
            drate=0.9,
            cost=log_likelihood,
            cost_deriv=log_likelihood_deriv_for_softmax,
            cutoff=100,
            #reg_beta=0.001,
            #reg=l2norm,
            #reg_deriv=l2norm_deriv,
            clip_gradient=10,
            #decay_frequency=40,
            #decay_rate=0.3,
        )
    

    rnn.add_layer(Layer(
        size=100,
        weight_factor=1,
        #activation=sigmoid,
        #activation_deriv=sigmoid_deriv
        ))
    #rnn.add_layer(Layer(
    #    size=5,
    #    weight_factor=1,
        #activation=sigmoid,
        #activation_deriv=sigmoid_deriv
    #    ))
    #rnn.add_layer(Layer(
    #    size=10,
    #    activation=identity,
    #    activation_deriv=identity_deriv,
    #    ))

    rnn.add_layer(Layer(
        size=2,
        activation=softmax,
        activation_deriv=softmax_deriv,
        recurrent=False,
        ))
    
    import time
    def every(nn,outputs,guesses):
        guess_dist = np.zeros(nn.layers[-1].size)
        correct = 0
        #print guesses[:,:,-1]
        for i,guess in enumerate(guesses):
            #guess = guess[-1]
            
            #if k % 1000 == 0:
            #    print "guess", guess
            #    print "output", outputs[i]
            #print "max: ",np.argmax(guess)
            
            #guess_dist[guess.ravel().argmax()] += 1
            #print "Guess",guess
            #print "Output",outputs[i]

            correct += np.sum(np.argmax(guess,axis=1) == np.argmax(outputs[i],axis=1))
            #print reconstruct_sentence(i,words,inputs),
            #print guess[-1]
        print "correct {}/{}".format(correct,guesses.shape[0] * 10)
        print "guess dist {}".format(guesses.round().sum(axis=1))
        print "guess mean {}".format(guesses[0].mean(axis=0))
        #print "true dist {}".format(outputs.sum(axis=0))
    #rnn = Network.load("eeg.rnn.npy")
    #rnn.lrate=0.000001
    #rnn.decay_frequency = 10000
    rnn.train(inputs,np.array(outputs),do_every=every,batch=100,every=30,iterations=100000)
    rnn.save("eeg.rnn")

#np.save("eeg_inputs",_in)
#np.save("eeg_outputs",_out)
