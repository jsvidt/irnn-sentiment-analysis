import numpy as np
import codecs

with codecs.open("processed.txt","w","utf-8") as o:
    labels = {}
    
    with codecs.open("stanfordSentimentTreebank/sentiment_labels.txt") as f:
        for l in f.readlines()[1:]:
            idx = int(l.split("|")[0].strip())
            _class = float(l.split("|")[1].strip())
            print _class
            if _class > 0.6:
                _class = 1
            elif 0.4 >= _class:
                _class = 0
            else:
                _class = 2
            labels[idx] = _class
    i = 0

    dictionary = {}
    with codecs.open("stanfordSentimentTreebank/dictionary.txt","r","UTF-8") as f:
        for l in f.readlines():
            l = l.strip().split("|")
            _class = labels[int(l[1].strip())]
            if _class in [0,1]:
                o.write(l[0] + "\t" + str(_class) + "\n")
                i += 1
            #dictionary[l[0].lower().strip().encode("ascii","ignore")] = _class

    split = np.zeros(i)
    #print dictionary
    j = 0
    with codecs.open("stanfordSentimentTreebank/datasetSplit.txt") as f:
        for l in f.readlines()[1:]:
            l = l.strip().decode("latin-1").split(",")
            
            if labels[int(l[0].strip())] in [0,1]:
                split[j] = int(l[1].strip())
                j += 1

    np.save("split.npy",split)
    print split

