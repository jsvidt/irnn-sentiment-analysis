import numpy as np
import codecs

with codecs.open("processed.txt","w","utf-8") as o:
    labels = {}
    with codecs.open("stanfordSentimentTreebank/sentiment_labels.txt") as f:
        for l in f.readlines()[1:]:
            idx = int(l.split("|")[0].strip())
            _class = float(l.split("|")[1].strip())
            print _class
            if _class > 0.6:
                _class = 1
            elif 0.4 >= _class:
                _class = 0
            else:
                _class = 2
            labels[idx] = _class
    i = 0

    dictionary = {}

    with codecs.open("stanfordSentimentTreebank/dictionary.txt","r","UTF-8") as f:
        for l in f.readlines():
            l = l.strip().split("|")
            _class = labels[int(l[1].strip())]
            #if _class in [0,1]:
                #o.write(l[0] + "\t" + str(_class) + "\n")
                #i += 1
            dictionary[l[0].lower().strip().encode("ascii","ignore")] = _class
            i+=1

	sent_ids_classes = dict({})
    with codecs.open("stanfordSentimentTreebank/datasetSentences.txt","r","utf-8") as f:
        for l in f.readlines()[1:]:
            l = l.strip().split("\t")
            #if labels[int(l[0].strip())] in [0,1]:
            idx = int(l[0].strip())
            sent = l[1].lower().strip().encode("ascii","ignore").replace("-lrb-","(").replace("-rrb-",")")
            classes = []
            #cum = ""
            #for w in sent.split(" "):
            #    #cum += w + " "
            #    classes.append(dictionary[w])
            #print classes
            try:
                _class = dictionary[sent]
                sent_ids_classes[idx] = _class
                if _class in [0,1]:
                    o.write("{}\t{}\n".format(sent,_class))
            except Exception as e:
                print "NO DICT", sent, l

    print sent_ids_classes
    split = []
    #print dictionary
    j = 0
    with codecs.open("stanfordSentimentTreebank/datasetSplit.txt") as f:
        for l in f.readlines()[1:]:
            l = l.strip().decode("latin-1").split(",")
            idx = int(l[0].strip())
            if sent_ids_classes[idx] in [0,1]:
                split.append(int(l[1].strip()))
                j += 1


    np.save("split.npy",np.array(split))
    print split

