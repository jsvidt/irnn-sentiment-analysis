# coding: UTF-8
import numpy as np
from kfold import kFoldCross

from helpers import *

class Network:
    @staticmethod
    def load(filename):
        return np.load(filename).item()
    
    def __init__(self,input_size,cost=squared_error,cost_deriv=squared_error_deriv,lrate=0.5,reg=None,reg_deriv=None,reg_beta=None,drate=0.9,lmin=1):
        self.layers = []
        self.input_size = input_size
        self.cost = cost
        self.cost_deriv = cost_deriv
        self.lrate = lrate
        self.reg = reg
        self.reg_deriv = reg_deriv
        self.reg_beta = reg_beta
        self.drate = drate
        self.lmin = lmin
    
    def add_layer(self,layer,weights=None):
        if len(self.layers) == 0:
            layer.parent_size = self.input_size
        else:
            layer.parent_size = self.layers[-1].size
        
        layer.init_weights()
        
        if weights != None:
            layer.weights = weights
        
        self.layers.append(layer)
    
    def train(self,inputs,outputs,iterations=50,do_every=None,batch=None,every=1):
            for i in xrange(iterations):
                cost_sum = 0
                guesses = []
                
                if batch:
                    indicies = np.random.random_integers(0,inputs.shape[0] - 1,size=(batch,))
                    batch_inputs = inputs[indicies,:]
                    batch_outputs = outputs[indicies]
                else:
                    batch_inputs = inputs
                    batch_outputs = outputs
                
                for j in xrange(len(batch_inputs)):
                    x = batch_inputs[j]
                    y = batch_outputs[j]
                    #print "guess", self.forward(x), "actual", y
                    guess = self.forward(x)
                    guesses.append(guess)
                    self.backprop(y)
                    cost_sum += np.sum(self.cost(guess,y))
                if do_every and i % every == 0:
                    do_every(self,batch_outputs,np.array(guesses))
                print "iteration", i, "mean cost",cost_sum / batch_inputs.shape[0], "total examples:", i * (batch or 1)
                print "weights: max: {}, min: {}, avg: {}".format(max([np.max(layer.weights) for layer in self.layers]),min([np.min(layer.weights) for layer in self.layers]),np.mean(np.array([np.min(layer.weights) for layer in self.layers])))
                self.update_weights(batch or inputs.shape[0])
                
    def update_weights(self,batch_size):
        for layer in self.layers:
            layer.update_weights(batch_size,self.lrate,self.drate,self.lmin)
    
    def forward(self,inputs):
        if inputs.size != self.input_size:
            raise Exception("Wrong input_size")
            
        self.inputs = inputs
        activations = inputs
        for layer in self.layers:
            #z = np.dot(layer.weights,activations) + layer.biases
            #layer.inputs = z
            #activations = layer.activation(z)            
            #layer.activations = activations
            activations = layer.calc_activations(activations)
        
        self.output = activations
        return activations
    
    def backprop(self,y):
        output_layer = self.layers[-1]
        deltas = self.cost_deriv(self.output,y) * output_layer.activation_deriv(output_layer.inputs)
        
        for l,layer in enumerate(self.layers[::-1]):
            #print "deltas",deltas
            l = len(self.layers) - 1 - l
            #print "layer",l
            if l == 0:
                a = self.inputs
            else:
                a = self.layers[l - 1].activations
            
            #for i in range(a.size):
            #    for j in range(deltas.size):
            #        layer.updated_weights[j,i] += self.lrate * -1 * a[i] * deltas[j]
            if self.reg_deriv:
                reg_term = self.reg_beta * self.reg_deriv(layer.weights)
            else:
                reg_term = 0
            
            #layer.weight_updates += - 1 * self.lrate * (np.outer(deltas,a) - reg_term)
            layer.weight_updates += (layer.calc_weight_deltas(deltas,a) + reg_term)
                
               
            #print "weights",layer.weights
            
            ### OBS!!: FOrkert udregning af biases nedenfor
            #layer.biases = deltas
                
            if l != 0:
                lminusone = self.layers[l - 1]
                deltas = np.dot(layer.weights.T,deltas) * lminusone.activation_deriv(lminusone.inputs)
                    
    
    def save(self,filename):
        np.save(filename,self)
        
        
class Layer:
    def __init__(self,size,activation=sigmoid,activation_deriv=sigmoid_deriv):
        self.size = size
        self.biases = np.zeros(size)#(np.random.rand(size) - 0.5) 
        self.activation = activation
        self.activation_deriv = activation_deriv
    
    def init_weights(self):
        self.weights = (np.random.rand(self.size,self.parent_size) - 0.5)
        self.weight_updates = np.zeros((self.size,self.parent_size))
        self.lrates = np.zeros(self.weights.shape)
        #np.ones(self.weights.shape)

    def update_weights(self,batch_size,lrate,drate,lmin):
        print "max weight update",(self.weight_updates / batch_size).max()
        #self.weights += -1 * lrate * self.weight_updates / batch_size
        
        dx = self.weight_updates / batch_size
        
        self.lrates = drate * self.lrates - lrate * dx
        self.weights += self.lrates
        
            
        #self.lrates = np.maximum(1,np.minimum(lmin,drate * self.lrates + (1 - drate) * dx**2))
        #self.weights += - lrate * (dx / (np.sqrt(self.lrates) + epsi))
        
        print "max,min,avg lrate:",self.lrates.max(),self.lrates.min(),self.lrates.mean()
        
        self.weight_updates[:,:] = 0

    def calc_activations(self,activations):
        z = np.dot(self.weights,activations) + self.biases
        self.inputs = z
        self.activations = self.activation(z)
        return self.activations

    def calc_weight_deltas(self,deltas,a):
        return np.outer(deltas,a)

        
"""        
nn = Network(2)

nn.add_layer(Layer(40))#,activation=relu,activation_deriv=relu_deriv))
#nn.add_layer(Layer(2,activation=relu,activation_deriv=relu_deriv))

nn.add_layer(Layer(1))

#nn.add_layer(Layer(1)


inputs = np.array([
    [0,1],[1,0],[1,1]
    ])

outputs = np.array([
    [1],[1],[0],[0]
    ])

nn.train(inputs,outputs)
"""

def imgs_to_zero_to_one(labels):
    nlabs = labels.argmax(axis=1)
    return nlabs / float(nlabs.max())

if __name__ == "__main__":
    """
    #inputs = np.load("faces.txt.npy")[:500]
    #outputs = np.load("labels.txt.npy")[:500] #imgs_to_zero_to_one(np.load("labels.txt.npy"))
    inputs = np.load("faces.npy")
    outputs = np.load("face_mf_labels.npy")
    print inputs.shape,outputs.shape
    
    print "data loaded"

    nn = Network(
        inputs[0].size,
        cost=log_likelihood,
        cost_deriv=log_likelihood_deriv,
        lrate=0.001,
        reg=l2norm,
        reg_deriv=l2norm_deriv,
        reg_beta = 0.1,
        )
    
    #nn.add_layer(Layer(40,activation=relu,activation_deriv=relu_deriv))
    nn.add_layer(Layer(40,activation=relu,activation_deriv=relu_deriv))
    nn.add_layer(Layer(40,activation=relu,activation_deriv=relu_deriv))
    #nn.add_layer(Layer(5,activation=relu,activation_deriv=relu_deriv))
    
    #nn.add_layer(Layer(10,activation=relu,activation_deriv=relu_deriv))
    nn.add_layer(Layer(20,activation=identity,activation_deriv=identity_deriv))#,activation=relu,activation_deriv=relu_deriv))
    #nn.add_layer(Layer(10))#,activation=relu,activation_deriv=relu_deriv))
    #nn.add_layer(Layer(20,activation=relu,activation_deriv=relu_deriv))
    #nn.add_layer(Layer(20,activation=relu,activation_deriv=relu_deriv))
    
    # Åbenbart er det en god idé at have et lag med en linearitet, før softmax output, hvis man bruger ReLU i de andre lag - ellers kommer der en masse nuller med fra reluerne
    nn.add_layer(Layer(outputs[0].size,activation=softmax,activation_deriv=softmax_deriv))#,activation_deriv=sigmoid_deriv_for_cross_entropy
    
    def do(nn,_outputs,guesses):
        correct = 0
        for i in xrange(inputs.shape[0]):
            r,g = np.argmax(outputs[i]),np.argmax(nn.forward(inputs[i]))
            #r,g = outputs[i] * 4,round(guess* 4)
            #print r,g,"sum_guess:",np.sum(guess) # uncomment to print answers
            correct += r==g
        print "correct {}/{}".format(correct,inputs.shape[0])

    #nn = Network.load("imgs.nn.npy")
    nn.train(inputs,outputs,do_every=do,iterations=4000,batch=10)
    
    nn.save("imgs.nn")
    """
    
    def createnn(input_size,output_size):
        nn = Network(
            input_size,
            cost=cross_entropy,
            cost_deriv=log_likelihood_deriv,
            lrate=0.0001,
            drate=0.9,
            lmin=10000,
            reg=l2norm,
            reg_deriv=l2norm_deriv,
            reg_beta = 0.1,
            )
        
        #nn.add_layer(Layer(100,activation=relu,activation_deriv=relu_deriv))
        #nn.add_layer(Layer(10,activation=relu,activation_deriv=relu_deriv))
        
        nn.add_layer(Layer(90,activation=relu,activation_deriv=relu_deriv))
        
        nn.add_layer(Layer(50,activation=relu,activation_deriv=relu_deriv))
        nn.add_layer(Layer(50,activation=relu,activation_deriv=relu_deriv))
        nn.add_layer(Layer(50,activation=relu,activation_deriv=relu_deriv))

        #nn.add_layer(Layer(50))
        #nn.add_layer(Layer(40))#,activation=relu,activation_deriv=relu_deriv))
        #nn.add_layer(Layer(20,activation=relu,activation_deriv=relu_deriv))
        #nn.add_layer(Layer(20,activation=relu,activation_deriv=relu_deriv))
        
        nn.add_layer(Layer(25,activation=identity,activation_deriv=identity_deriv))#,activation=relu,activation_deriv=relu_deriv))
        nn.add_layer(Layer(output_size,activation=softmax,activation_deriv=softmax_deriv))#,activation_deriv=sigmoid_deriv_for_cross_entropy
        return nn
    
    def check(nn,outputs,guesses):
        correct = 0
        guess_dist = np.zeros(outputs.shape)
        sum_cost = 0
        for i,guess in enumerate(guesses):
            r,g = np.argmax(outputs[i]),np.argmax(guess)
            guess_dist[i,g] += 1
            sum_cost += np.sum(nn.cost(guess,outputs[i]))
            #r,g = outputs[i] * 4,round(guess* 4)
            #print r,g,"sum_guess:",np.sum(guess) # uncomment to print answers
            correct += r==g

        print "correct {}/{}".format(correct,guesses.shape[0]),
        print "mean cost {}".format(sum_cost / guesses.shape[0]),
        print ",true distribution: {}".format(np.sum(outputs,axis=0)),
        print ",answer distribution: {}".format(np.sum(guess_dist,axis=0))
    
    # HVAD laver du?

    #kFoldCross(createnn,"imgs.txt.npy","labels.txt.npy",check=check)
    kFoldCross(createnn,
        "faces.npy",
        #"imgs.txt.npy",
        "face_mf_labels.npy",
        #"labels.txt.npy",
        #"eeg_inputs.npy",
        #"eeg_outputs.npy",
        check=check,
        batch=None,
        iterations=1000,
        folds=10,
        super_iterations=1,
        #save_in="mnist_validation",
        save_in="faces_3_validation",
        #max_inputs=500
        )
    
    

#nn.add_layer(Layer(1)


#print relu(np.array([1,-1,10]))

#for i in range(10000):
#    for t in training:
#        print 'output_forward:', nn.forward(t[0])

#        nn.backprop(t[1])
    
#        print "\n"

#     [2]
#     [0]
#[1,0][2]
#[0,0][0]
#
#2x2 2x1

#2x1

# [0,1]
