import numpy as np
from rnn import *

class Ensemble():
    def __init__(self,size=5,sort=False):
        self.community = []
        self.size = size
        self.sort = sort
    
    def add_member(self,nn):
        self.community.append(nn)
        if self.sort:
            self.community = sorted(self.community,key=lambda x: x.certainty)[::-1][:self.size]
        elif len(self.community) > self.size:
            del self.community[0]
        nn.cache = dict()
        print "Community", [x.certainty for x in self.community]
    
    @staticmethod
    def create_from_files(files):
        ensemble = Ensemble()
        for f in files:
            ensemble.add_member(Network.load(f))

        return ensemble

    def forward(self,input,key=None):
         guesses = np.zeros(self.community[0].layers[-1].size)
         for member in self.community:
             if key != None and member.cache.has_key(key):
                 idx = member.cache[key]
             else:
                 idx = member.forward(input)[-1]#.argmax()
                 if key: member.cache[key] = idx
             guesses[idx.argmax()] += idx.max() * ((member.certainty / 100 )**2)
         #guess = np.array(guesses).mean(axis=1)
         return guesses
