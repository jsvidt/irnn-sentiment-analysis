import codecs

def process_data():
    with codecs.open("processed.txt","w","latin-1") as f:

        with codecs.open("plot.tok.gt9.5000","r","utf-8") as g:
            for l in g.readlines():
                try:
                    #l = codecs.decode(l,"latin-1")
                    f.write(l.strip() + u"\t 0 \n")
                except UnicodeDecodeError:
                    pass
        
        with codecs.open("quote.tok.gt9.5000","r") as g:
        
            for l in g.readlines():
                #l = codecs.decode(l,"latin-1")
                try:
                    f.write(l.strip() + u"\t 1 \n")
                except UnicodeDecodeError:
                    pass
        """

        with codecs.open("../sentiment/imdb_labelled.txt") as g:
            for l in g.readlines():
                #l = codecs.decode(l,"latin-1")
                try:
                    splt = l.split(u"\t")
                    cls = splt[-1]
                    f.write(u" ".join(splt[:-1]).strip() + u"\t {} \n".format(cls))
                except UnicodeDecodeError:
                    print l
                    pass
        with codecs.open("../UMICH.SI650/training.txt") as g:
            for l in g.readlines():
                #l = codecs.decode(l,"latin-1")
                try:
                    splt = l.split(u"\t")
                    cls = splt[0]
                    f.write(u" ".join(splt[1:]).strip() + u"\t {} \n".format(cls))
                except UnicodeDecodeError:
                    print l
                    pass
        """

if __name__ == "__main__":
    process_data()
