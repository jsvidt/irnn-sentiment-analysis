from rnn import *

nn = Network.load("sentiment.rnn.npy")

from sentiment.loadsentences import *

words,inputs,outputs = loadsentences(number=1000)
print nn.input_size,inputs[0].shape

values = {
    0: "Negative",
    1: "Positive"
}

for i,_in in enumerate(inputs):
    guess = values[np.argmax(nn.forward(_in)[-1])]
    out = values[np.argmax(outputs[i])]
    if guess == out:
        right_or_wrong = "RIGHT"
    else:
        right_or_wrong = "WRONG"
    print "{}: Guess: {}, Ground Truth: {}, Sent: {}\n".format(right_or_wrong,guess,out,reconstruct_sentence(i,words,inputs))