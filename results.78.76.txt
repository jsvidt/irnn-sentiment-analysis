    rnn = Network(
            input_size=len(words),
            lrate=0.001,
            drate=0.9,
            cost=log_likelihood,
            cost_deriv=log_likelihood_deriv_for_softmax,#log_likelihood_deriv,
            cutoff=4000,
            #reg_beta=0.000001,
            #reg=l2norm,
            #reg_deriv=l2norm_deriv,
            clip_gradient=100,
            onehot_input=True,
            decay_frequency=10000,
            decay_rate=0.5,
            dropout=0.45,
            )
    rnn.add_layer(Layer(
        size=160,
        #activation=sigmoid,
        #activation_deriv=sigmoid_deriv
        ))
    rnn.add_layer(Layer(
        size=40,
    #    activation=identity,
    #    activation_deriv=identity_deriv
        ))

    kFoldCross(createnn,
    "sentence_inputs_dok.npy",
    "sentence_outputs_dok.npy",
    every,
    folds=5,
    iterations=100000,
    every=50,
    batch=20,
    super_iterations=1,
    save_in="rotten_validation")

         Training: 100.0 %
         Validation: 78.7623066104 %
         
