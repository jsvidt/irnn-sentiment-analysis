import os
import codecs

#output = []
#labels = []
with codecs.open("processed.txt","w") as o:
    for label,pol in enumerate(["neg","pos"]):
        path = "txt_sentoken/{}".format(pol)
        for n in os.listdir(path):
            with codecs.open(os.path.join(path,n)) as f:
                val = " ".join([l.strip() for l in f.readlines()])
                o.write("{}\t{}\n".format(val,label))
