import numpy as np
from matplotlib import pyplot as plt
import time
from nn import *

def plot_imgs(nn,imgs,labels,size=(28,28)):
    #plt.ion()
    
    plt.subplot(221)
    _input = plt.imshow(imgs[0].reshape(size))
    text = plt.text(1,1,"hejsa",color="white",size="large")

    plt.subplot(222)
    activations = plt.imshow(nn.layers[-1].activations[np.newaxis,:])
    #plt.show()
    for i in xrange(imgs.shape[0]):
        #time.sleep(20)
        guess = nn.forward(imgs[i])
        _input.set_data(imgs[i].reshape(size))

        activations.set_data(nn.layers[-1].activations[np.newaxis,:])

        text.set_text("guess: {}, actual: {}".format(guess.argmax(),labels[i].argmax()))
        plt.draw()
        plt.pause(1)
        

#imgs = np.load("imgs.txt.npy")
#labels = np.load("labels.txt.npy")
imgs = np.load("mnist_validation/0_input_test.npy")
labels = np.load("mnist_validation/0_output_test.npy")
nn = np.load("mnist_validation/0_nn.npy").item()

plot_imgs(nn,imgs,labels,size=(28,28))
