from sklearn.cross_validation import KFold
import numpy as np

import os
from ensemble import *
#f = "imgs.txt.npy"

def kFoldCross(createnn,file,labels_file,check,iterations=500,batch=None, folds=5,super_iterations=0,save_in=None,max_inputs=None,every=1,random_state=10,kf=None,flip=False):
    imgs = np.load(file)
    labels = np.load(labels_file)
    print "Loaded"
    #print imgs.shape,labels.shape
    if max_inputs:
        imgs = imgs[:max_inputs]
        labels = labels[:max_inputs]
    if kf == None:
        kf = KFold(len(imgs), n_folds=folds, shuffle=True, random_state=random_state)

    if super_iterations:
        super_iterations = min(len(kf),super_iterations)
    else:
        super_iterations = len(kf)
    
    if save_in:
        if not os.path.exists(save_in):
            os.makedirs(save_in)
    kf = list(kf)
    for i in xrange(super_iterations):
        final_index = None
        if len(kf[i]) > 2:
            train_index, test_index, final_index = kf[i]
        else:
            train_index, test_index = kf[i]
        
        nn = createnn()
        nn.best_validation_iteration = 0
        ensemble = Ensemble()
        print train_index.shape,test_index.shape
        def save():
            name = os.path.join(save_in,"{}_".format(i))
            nn.save(name + "nn")
            
            np.save(name + "input_train",img_train)
            np.save(name + "input_test",img_test)
            
            np.save(name + "output_train",label_train)
            np.save(name + "output_test",label_test)
            
            np.save(name + "ensemble_test",ensemble)
            if final_index != None:
                np.save(name + "input_final",img_final)
                np.save(name + "output_final",label_final)                

        #print("TRAIN:", train_index, "TEST:", test_index)
        img_train, img_test = imgs[train_index], imgs[test_index]
        label_train, label_test = labels[train_index], labels[test_index]

        if final_index != None:
            img_final = imgs[final_index]
            label_final = labels[final_index]

        def do_every(nn,outputs,guesses):
            test_guesses = []
            for j in xrange(img_test.shape[0]):
                ff = nn.forward(img_test[j])
                #bw = nn.forward(img_test[j][::-1])
                test_guesses.append(ff)
            
            print "Training",
            check(nn,outputs,guesses,"training",img_train,ensemble)
            
            print "Validation",
            check(nn,label_test,np.array(test_guesses),"validation",img_test,ensemble)

            if final_index != None:
                final_guesses = []
                for j in xrange(img_final.shape[0]):
                    final_guesses.append(nn.forward(img_final[j]))    

                print "Final",
                check(nn,label_final,np.array(final_guesses),"final",img_final,ensemble)

            save()
            
        Network.train(nn,img_train,label_train,iterations=iterations,batch=batch,do_every=do_every,every=every,flip=flip)
        save()
        
    #return img_train, img_test
    #print len(img_train), len(test_index)
   
#train, test  = kFoldCross(f)
#print len(train), len(test)


#kFoldCross("imgs.txt.npy","labels.txt.npy",check=check)
