import numpy as np

epsi = np.finfo(float).eps

def cross_entropy(a,y):
    return -y * np.log(a + epsi)  - (1-y) * np.log(1 - a + epsi)
    
def cross_entropy_deriv(a,y):
    return (a-y) / (a*(1 - a) + epsi)

def cross_entropy_hat(a,y):
    y , a = highest(a,y)
    return -y * np.log(a + epsi)
    
def cross_entropy_deriv_hat(a,y):
    y , a = highest(a,y)
    return float(-y) / (a + epsi)
    
def highest(x,y):
    if x > y: 
        return x, y 
    else:
        return y, x
        
x = 0
y = 1

print '0,1:'
print cross_entropy(x,y)
print cross_entropy_deriv(x,y)
print cross_entropy_hat(x,y)
print cross_entropy_deriv_hat(x,y)

print '1,0:'
print cross_entropy(y,x)
print cross_entropy_deriv(y,x)
print cross_entropy_hat(y,x)
print cross_entropy_deriv_hat(y,x)


print "bambus", cross_entropy_deriv(1,0)
