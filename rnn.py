import numpy as nn

from helpers import *

import math

from sklearn import utils

from scipy import sparse

import numexpr as ne

class NetworkInterface:
    def activate_dropout(self):
        raise Exception("Networks must implement activate_dropout method")

    def deactivate_dropout(self):
        raise Exception("Networks must implement deactivate_dropout method")

    def dropout_scale(self):
        raise Exception("Networks must implement dropout_scale method")
    
    def dropout_rescale(self):
        raise Exception("Networks must implement dropout_rescale method")

    def forward(self):
        raise Exception("Networks must implement forward method")

    def backprop(self):
        raise Exception("Networks must implement backprop method")

    def update_weights(self):
        raise Exception("Networks must implement update_weights method")

    def inspect(self):
        raise Exception("Networks must implement inspect method")

class BIRNN(NetworkInterface):
    def __init__(self,frnn,brnn,onn,verbose=False,dropout=0.5):
        self.verbose = verbose
        self.frnn = frnn
        self.brnn = brnn
        self.frnn.cost = connect
        self.frnn.cost_deriv = connect_deriv
        self.frnn.dropout_last = True

        self.brnn.cost = connect
        self.brnn.cost_deriv = connect_deriv
        self.brnn.dropout_last = True

        self.dropout = dropout
        self.reg_beta = self.reg_beta_r = self.reg_beta_b = 0

        self.decay_rate = None
        self.decay_frequency = None

        self.onn = onn
        self.layers = frnn.layers + brnn.layers + onn.layers

        self.cost = onn.cost

        self.rnn_output_size = rnn_output_size = self.frnn.layers[-1].size
        if rnn_output_size != self.brnn.layers[-1].size:
            raise Exception("Lowest layers of rnns must be same sizes")

        if rnn_output_size * 2 != onn.input_size:
            raise Exception("Lowest layers of rnns must be half the size of input_size of output network")
        

    def save(self,name):
        np.save(name,self)
    
    def forward(self,inputs):
        f = self.frnn.forward(inputs)[-1]
        b = self.brnn.forward(inputs.copy()[::-1])[-1]
        if self.verbose:
            print f,b
        o_inputs = np.array([np.concatenate((f,b))])
        if self.verbose:
            print "o_inputs shape",o_inputs.shape,o_inputs
        o = self.onn.forward(o_inputs)
        return o

    def backprop(self,outputs,batch_size=1,dropout=0):
        o_deltas = self.onn.backprop(outputs,batch_size=batch_size,dropout=dropout)
        r_deltas = np.dot(self.onn.layers[0].forward_weights.T,o_deltas)
        f_deltas,b_deltas = r_deltas[:self.rnn_output_size],r_deltas[self.rnn_output_size:]
        
        self.frnn.backprop(np.array([f_deltas]),batch_size=batch_size,dropout=dropout)
        self.brnn.backprop(np.array([b_deltas]),batch_size=batch_size,dropout=dropout)

    def update_weights(self,batch_size):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.update_weights(batch_size)

    def activate_dropout(self):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.activate_dropout()

    def deactivate_dropout(self):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.deactivate_dropout()
    
    def dropout_scale(self):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.dropout_scale()

    def dropout_rescale(self):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.dropout_rescale()

    def inspect(self):
        for nn in [self.frnn,self.brnn,self.onn]:
            nn.inspect()



class Network(object):
    @staticmethod
    def load(filename):
        return np.load(filename).item()
    
    def __init__(self,input_size,cost=squared_error,cost_deriv=squared_error_deriv,lrate=0.001,reg=None,reg_deriv=None,reg_beta=None,reg_beta_r=None,reg_beta_b=None,drate=0.9,lmin=1,cutoff=100000,dropout=None,clip_gradient=1000,decay_frequency=None,decay_rate=None,verbose=False,onehot_input=False,dropout_last=False):
        self.layers = []
        self.input_size = input_size
        self.cost = cost
        self.cost_deriv = cost_deriv
        self.lrate = lrate
        self.reg = reg
        self.reg_deriv = reg_deriv
        self.reg_beta = reg_beta
        self.reg_beta_r = reg_beta_r
        self.reg_beta_b = reg_beta_b
        self.drate = drate
        self.lmin = lmin
        self.cutoff = cutoff
        self.clip_gradient = clip_gradient
        self.decay_frequency = decay_frequency
        self.decay_rate = decay_rate
        self.verbose = verbose
        self.onehot_input = onehot_input
        self.dropout = dropout
        self.dropout_last = dropout_last

    def __str__(self):
        s = """
Network:
    Input size:{0}
    Cost: {1}
    Cost deriv: {2}
    L-rate: {3}
    Regularization: {4}
    Reg deriv: {5}
    Reg beta forward: {6}
    Reg beta recurrent: {7}
    Reg beta biases: {8}
    RMSProp decay-rate: {9}
    Cutoff: {10}
    Clip gradient: {11}
    Decay frequency: {12}
    Decay rate: {13}
    Onehot input: {14}
    Dropout value: {15}
    Dropout last layer: {16}
        """.format(
            self.input_size,
            self.cost,
            self.cost_deriv,
            self.lrate,
            self.reg,
            self.reg_deriv,
            self.reg_beta,
            self.reg_beta_r,
            self.reg_beta_b,
            self.drate,
            self.cutoff,
            self.clip_gradient,
            self.decay_frequency,
            self.decay_rate,
            self.onehot_input,
            self.dropout,
            self.dropout_last
            )
        for layer in self.layers:
            s += "\n" + str(layer)
        return s

    def add_layer(self,layer,weights=None):
        if not layer.parent_size:
            print "NO PARENT SIZE"
            if len(self.layers) == 0:
                layer.parent_size = self.input_size
            else:
                layer.parent_size = self.layers[-1].size
        
        layer.init_weights()
        
        if weights != None:
            layer.weights = weights
        
        self.layers.append(layer)
        return layer
    
    def activate_dropout(self):
        if self.dropout:
            if self.dropout_last:
                layers = self.layers
            else:
                layers = self.layers[:-1]

            for layer in layers:
                if not layer.do_dropout:
                    continue
                idx = np.arange(layer.size)
                np.random.shuffle(idx)
                amount = int(round(float(len(idx)) * self.dropout))
                layer.drop_forward = idx[np.random.rand(layer.size) > self.dropout]
                

                predrop_forward_zeros = (layer.forward_weights == 0).sum()
                layer.droppped_forward = layer.forward_weights[layer.drop_forward,:].copy()
                layer.forward_weights[layer.drop_forward,:] = 0
                postdrop_forward_zeros = (layer.forward_weights == 0).sum()

                if self.verbose:
                    print "Forward dropped,", layer.droppped_forward,layer.drop_forward
                    print "Forward amount dropped,", amount, layer.drop_forward.shape, layer.drop_forward.shape, " of ", idx.shape[0]
                    print "Forward zeros, pre:", predrop_forward_zeros, "post:",postdrop_forward_zeros
                    #print "Forward zeros diff:", (postdrop_forward_zeros) - ( + (layer.droppped_forward == 0).sum())

                layer.drop_recurrent = layer.drop_forward.copy()#idx[:amount]
                #layer.drop_recurrent = layer.drop_recurrent[:,0],layer.drop_recurrent[:,1]

                layer.droppped_recurrent = layer.recurrent_weights[:,layer.drop_recurrent].copy()
                layer.recurrent_weights[layer.drop_recurrent,:] = 0

                if self.verbose:
                    print "recurrent dropped,", layer.droppped_recurrent,layer.drop_recurrent
                    print "recurrent amount dropped,", amount, layer.drop_recurrent.shape, layer.drop_recurrent.shape, " of ", idx.shape[0]

                
                layer.droppped_biases = layer.biases[layer.drop_forward].copy()
                layer.biases[layer.drop_forward] = 0


    def dropout_scale(self):
        if self.dropout:
            if self.dropout_last:
                layers = self.layers
            else:
                layers = self.layers[:-1]

            for layer in layers:
                if not layer.do_dropout:
                    continue
                layer.orig_forward_weights = layer.forward_weights.copy()
                layer.forward_weights *= (1 - self.dropout)
                layer.orig_recurrent_weights = layer.recurrent_weights.copy()
                layer.recurrent_weights *= (1 - self.dropout)
    def dropout_rescale(self):
        if self.dropout:
            if self.dropout_last:
                layers = self.layers
            else:
                layers = self.layers[:-1]

            for layer in layers:
                if not layer.do_dropout:
                    continue
                layer.forward_weights = layer.orig_forward_weights.copy()
                layer.recurrent_weights = layer.orig_recurrent_weights.copy()


    def deactivate_dropout(self):
        if self.dropout:
            #self.dropout_active = False
            #return 
            if self.dropout_last:
                layers = self.layers
            else:
                layers = self.layers[:-1]

            for layer in layers:
                if not layer.do_dropout:
                    continue
                layer.forward_weights[layer.drop_forward,:] = layer.droppped_forward.copy()
                layer.recurrent_weights[:,layer.drop_recurrent] = layer.droppped_recurrent.copy()
                layer.biases[layer.drop_forward] = layer.droppped_biases.copy()

    @staticmethod
    def train(self,inputs,outputs,iterations=50,do_every=None,batch=None,every=1,flip=False):
            self.every = every
            #self.sparse = sparse.issparse(inputs[0])
            epoch_sum = 0
            for i in xrange(iterations):
                if self.decay_frequency and (i + 1) % self.decay_frequency == 0:
                    self.lrate *= self.decay_rate
                    print "decaying lrate: ",self.lrate
                cost_sum = 0
                guesses = []
                self.i = i
                
                self.activate_dropout()

                if batch:
                    indicies = np.random.random_integers(0,inputs.shape[0] - 1,size=(batch,))
                    batch_inputs = inputs[indicies]
                    batch_outputs = outputs[indicies]
                else:
                    batch_inputs = inputs
                    batch_outputs = outputs
                if flip and np.random.rand() > 0.5:
                    print "flipping"
                    if self.onehot_input:
                        batch_inputs = batch_inputs.copy()
                        for j in xrange(batch_inputs.size):
                            batch_inputs[j] = batch_inputs[j][::-1]
                    else:
                        batch_inputs = batch_inputs.copy()[:,::-1,:]
                for j in xrange(len(batch_inputs)):
                    #if self.sparse and not self.onehot_input:
                    #    x = batch_inputs[j].toarray()
                    #else:
                    x = batch_inputs[j]
                    y = batch_outputs[j]
                    #print "guess", self.forward(x), "actual", y
                    guess = self.forward(x)
                    guesses.append(guess)
                    self.backprop(y,batch_size=batch_inputs.shape[0],dropout=self.dropout)
                    cost_sum += np.sum(self.cost(guess[-1],y))

                self.update_weights(batch or inputs.shape[0])
                self.deactivate_dropout()

                epoch_sum += cost_sum / batch_inputs.shape[0]
                
                if do_every and (i + 1) % self.every == 0:
                    #self.dropout_scale()
                    print "Epoch mean cost:",epoch_sum / self.every
                    epoch_sum = 0
                    do_every(self,batch_outputs,np.array(guesses))
                    #self.dropout_rescale()
                
                print "iteration", i, "mean cost",cost_sum / batch_inputs.shape[0], "epoch mean cost", epoch_sum / ((i + 1) % every + 1), "total examples:", i * (batch or 1)
                #print "weights: max: {}, min: {}, avg: {}".format(max([np.max(layer.weights) for layer in self.layers]),min([np.min(layer.weights) for layer in self.layers]),np.mean(np.array([np.min(layer.weights) for layer in self.layers])))
    
    def inspect(self):
        for layer in self.layers:
            layer.inspect()
                
    def update_weights(self,batch_size):
        for layer in self.layers:
            layer.update_weights(batch_size,self.lrate,self.drate,self.clip_gradient)

        if self.reg_deriv:
            for layer in self.layers:
                layer.forward_weights -= self.reg_beta * self.reg_deriv(layer.forward_weights) / batch_size
                if self.reg_beta_r:
                    layer.recurrent_weights -= self.reg_beta_r * self.reg_deriv(layer.recurrent_weights) / batch_size
                if self.reg_beta_b:
                    layer.biases -= self.reg_beta_b * self.reg_deriv(layer.biases) / batch_size

    
    def forward(self,inputs):
        """
            input shape is 
            [
                [input vector at time t],
                [input vector at time t+1],
                ...
                [input vector at time t+n],
            ]
        """
        #print inputs.shape
        if not self.onehot_input and inputs.shape[1] != self.input_size:
            raise Exception("Wrong input_size")
        
        self.inputs = inputs
        #self.inputs = []
        #for _in in inputs:
        #    ain = np.zeros(self.input_size)

        #    ain[_in] = 1
        #    self.inputs.append(ain)

        #self.inputs = np.array(self.inputs)

        time_steps = inputs.shape[0] # timesteps = number of rows in input matrix
        
        previous_activations = [np.zeros(layer.size) for layer in self.layers]
        
        output_layer = self.layers[-1]
        
        self.output = np.zeros((time_steps,output_layer.size))
        
        for layer in self.layers:
            layer.inputs = np.zeros((time_steps,layer.size))
            layer.activations = np.zeros((time_steps,layer.size))
        for time in xrange(time_steps):
            activations = self.inputs[time] # activations of input layer at time t

            for l in xrange(len(self.layers)):
                layer = self.layers[l]
                #print "activations shape",activations.shape,layer.forward_weights.T.shape
                #.reshape((layer.forward_weights.shape[0],)#
                if l == 0 and self.onehot_input:
                    index = inputs[time]#activations
                    if index == None:
                        z = layer.biases.copy()
                    else:
                        z = layer.forward_weights[:,index].copy() + layer.biases
                else:
                    z = np.dot(layer.forward_weights,activations) + layer.biases
                    #print layer.forward_weights.shape,l,z
                #z = activations.dot(layer.forward_weights.T).flatten() #+ layer.biases
                #print "z shape",z.shape
                #if l < len(self.layers) - 1:
                if layer.recurrent:
                    z += np.dot(layer.recurrent_weights,previous_activations[l])
                #z = np.nan_to_num(z)
                layer.inputs[time,:] = z
                activations = layer.activation(z)  
                #if layer != output_layer  and self.dropout and self.dropout_active:
                #    activations *= layer.dropout_vector#(np.random.rand(layer.size) <= self.dropout) * 1
                #    print "layer",l,activations.shape,layer.dropout_vector.shape,(activations > 0).sum(),layer.dropout_vector.sum()
                layer.activations[time,:] = activations
                previous_activations[l] = activations.copy()
            self.output[time] = activations.copy()
        return self.output
    
    def backprop(self,y,batch_size=1,dropout=None):
        output_layer = self.layers[-1]
        if output_layer.size != y.shape[1]:
            raise Exception("Wrong output size for vector " + str(y))
        
        time_steps = self.inputs.shape[0]

        in_out_diff = max(0,time_steps - y.shape[0])

        time_range = range(in_out_diff,time_steps)[::-1]

        #forward_gradients = []
        #recurrent_gradients = []

        #for layer in self.layers:
        #    forward_times = [np.zeros(layer.forward_weight_updates.shape) for i in time_range]
        #    recurrent_times = [np.zeros(layer.recurrent_weight_updates.shape) for i in time_range]
        #    forward_gradients.append(forward_times)
        #    recurrent_gradients.append(recurrent_times)
        total_steps = 0
        _deltas = []
        for time in time_range:
            if self.verbose:
                print "\n\nouter time",time
                print "output, guess",y[time - in_out_diff],self.output[time]
            previous_deltas = [np.zeros(layer.size) for layer in self.layers]
            #previous_deltas[-1] =  
            #print "time",time
            steps_at_time = 0
            for bptt_step in range(max(0,time - self.cutoff),time+1)[::-1]:
                if self.verbose:
                    print "bptt_step", bptt_step
                if bptt_step == time:
                    if self.verbose:
                        print "first step"
                    
                    deltas = self.cost_deriv(self.output[time],y[time - in_out_diff])

                    if self.verbose:
                        print "cost_deriv for first step",deltas
                    deltas *= output_layer.activation_deriv(output_layer.inputs[time])
                else:
                    if output_layer.recurrent:
                        deltas = np.dot(output_layer.recurrent_weights,previous_deltas[-1]) * output_layer.activation_deriv(output_layer.inputs[bptt_step])
                    else:
                        deltas = np.zeros(output_layer.size)
                #else:
                #    zeros = np.zeros(previous_deltas[-1].shape)# output_layer.activation_deriv(output_layer.inputs[time])#
                #print "first last"
                #else:
                #    if self.verbose:
                #        print "next step"
                #    deltas = previous_deltas[-1]
                    
                #print "time:", time, "bptt_step:", bptt_step
                for i,layer in enumerate(self.layers[::-1]):
                    l = len(self.layers) - 1 - i
                    # if we're not at the first time step of this bptt loop, include deltas from last timestep, in this delta
                    """
                    if bptt_step != time: #and bptt_step != 0:
                        #print "inputs shape previous step", self.inputs[bptt_step - 1].shape
                        pass
                        # TODO: check that the stuff below fits, specifically bptt_step - 1. Shouldn't it be current input?
                        # or is it input at previous time step, i.e. bptt_step + 1
                        #deltas += np.dot(layer.recurrent_weights,previous_deltas[l]) * layer.activation_deriv(layer.inputs[bptt_step - 1])
                        #previous_inputs = layer.inputs[bptt_step]
                        #
                        #deltas += np.dot(layer.recurrent_weights,previous_deltas[l]) * layer.activation_deriv(previous_inputs)
                        #if self.verbose:
                        #    print "previous_inputs", previous_inputs
                        #    print "deltas", deltas
                        #np.add(deltas,np.dot(layer.recurrent_weights,previous_deltas[l]) * layer.activation_deriv(previous_inputs),deltas)
                    else:
                        # for every timestep step, add regularization to weight updates
                        pass
                    """
                    if self.verbose:
                        print "\n\nlayer",l
                        print "deltas", deltas

                    
                        
                    #print "deltas",deltas
                    
                    
                    if l == 0:
                        #if bptt_step == time:
                        #    print "AT TIME"
                        upper_a = self.inputs[bptt_step]
                        if self.verbose:
                            print "upper_a from inputs", upper_a
                        #else:
                        #    upper_a = np.zeros(self.inputs[bptt_step].shape)
                    else:
                        if self.verbose:
                            print "upper_a from layer", l-1
                        upper_a = self.layers[l - 1].activations[bptt_step]
                    
                    if self.verbose:
                        print "forward_activations",upper_a
                    
                    # recurrent_a is activations from prev timestep of this layer
                    if bptt_step == 0 or (not layer.recurrent):
                        if self.verbose:
                            print "Setting recurrent_activations to zero"
                        recurrent_a = np.zeros(layer.size)#np.ones(layer.size)
                    else:
                        recurrent_a = layer.activations[bptt_step - 1]
                    
                    if self.verbose:
                        print "recurrent:", layer.recurrent
                        print "recurrent_activations",recurrent_a
                    
                    #for i in range(a.size):
                    #    for j in range(deltas.size):
                    #        layer.updated_weights[j,i] += self.lrate * -1 * a[i] * deltas[j]
                    
                    """
                    if self.reg_deriv:
                        #if self.onehot_input and l == 0:
                        #    ws = layer.forward_weights[:,upper_a]
                        #else:
                        ws = layer.forward_weights
                        reg_term_forward = self.reg_beta * self.reg_deriv(ws)
                    else:
                        reg_term_forward = 0
                    
                    if self.reg_deriv:
                        reg_term_recurrent = self.reg_beta * self.reg_deriv(layer.recurrent_weights)
                    else:
                        reg_term_recurrent = 0
                    """
                    if self.verbose:
                        print "deltas shape", deltas.shape
                        #if upper_a != None:
                        #    print "upper_a shape", upper_a.shape
                    if self.onehot_input and l == 0:
                        #fw_gradients = np.zeros(layer.forward_weights.shape)
                        #fw_gradients[:,upper_a] = deltas.T
                        if upper_a != None:
                            layer.forward_weight_updates_temp[:,upper_a] += deltas.T / batch_size #+ reg_term_forward
                    else:
                        fw_gradients = np.outer(deltas,upper_a)
                        #fw_gradients += reg_term_forward
                        layer.forward_weight_updates_temp += fw_gradients / batch_size
                        #ne.evaluate("fwu + fw_gradients",out=layer.forward_weight_updates,local_dict={"fwu":layer.forward_weight_updates,"fw_gradients":fw_gradients})
                        #np.add(layer.forward_weight_updates,fw_gradients,out=layer.forward_weight_updates)
                    
                    if layer.recurrent:
                        rw_gradients = np.outer(deltas,recurrent_a) #+ reg_term_recurrent
                        layer.recurrent_weight_updates_temp += rw_gradients / batch_size
                    #ne.evaluate("rwu + rw_gradients",out=layer.recurrent_weight_updates,local_dict={"rwu":layer.recurrent_weight_updates,"rw_gradients":rw_gradients})
                    #np.add(layer.recurrent_weight_updates,rw_gradients,out=layer.recurrent_weight_updates)

                    layer.bias_updates += deltas / batch_size
                    
                    previous_deltas[l] = deltas.copy()
                    if l != 0:
                        lminusone = self.layers[l - 1]
                        
                        # save_deltas for next bptt iteration
                        ## set deltas for layer l - 1, that is the layer above this layer
                        
                        # TODO: the stuff below should be inputs at current time in backprop? Now its input at "outer" time. Should be current time, bptt
                        #deltas = np.dot(layer.forward_weights.T,deltas) * lminusone.activation_deriv(lminusone.inputs[time])
                        # CHANGED FROM ABOVE: 
                        if self.verbose:
                            print "updating deltas"
                        
                        deltas = np.dot(layer.forward_weights.T,deltas) * lminusone.activation_deriv(lminusone.inputs[bptt_step])
                        
                        if lminusone.recurrent:
                            previous_inputs = lminusone.inputs[bptt_step]
                            deltas += np.dot(lminusone.recurrent_weights,previous_deltas[l-1]) * lminusone.activation_deriv(previous_inputs)
                            if self.verbose:
                                print "previous_inputs", previous_inputs
                                print "deltas", deltas
                    else:
                        _deltas.append(deltas)



                total_steps += 1
                """                
                for layer in self.layers:
                    layer.forward_weight_updates /= (1 + steps_at_time)
                    layer.recurrent_weight_updates /= (1 + steps_at_time)
                """
        for layer in self.layers:
            layer.forward_weight_updates += layer.forward_weight_updates_temp #/ total_steps
            layer.forward_weight_updates_temp[:,:] = 0
            layer.recurrent_weight_updates += layer.recurrent_weight_updates_temp #/ total_steps
            layer.recurrent_weight_updates_temp[:,:] = 0
        return deltas#None,None,#forward_gradients,recurrent_gradients

    
    def save(self,filename):
        np.save(filename,self)


class Connector(NetworkInterface):
    def __init__(self,top,bottom,verbose=False,dropout=0.5,mean_pool=False):
        self.verbose = verbose
        self.top = top
        self.mean_pool = mean_pool
        self.bottom = bottom
        self.top.cost = connect
        self.top.cost_deriv = connect_deriv
        self.top.dropout_last = True
        
        self.dropout = dropout
        self.reg_beta = self.reg_beta_r = self.reg_beta_b = 0

        self.decay_rate = None
        self.decay_frequency = None

        self.bottom = bottom
        self.layers = top.layers + bottom.layers

        self.cost = bottom.cost
        self.cost_deriv = bottom.cost_deriv

        self.top_size = top_size = self.top.layers[-1].size
        if top_size != self.bottom.input_size:
            raise Exception("Top must give output of size equal to bottoms input size")
        

    def save(self,name):
        np.save(name,self)
    
    def forward(self,inputs):
        self.input_length = len(inputs)
        if not self.mean_pool:
            t = self.top.forward(inputs)[-1]
        else:
            t = self.top.forward(inputs).mean(axis=0)
        
        if self.verbose:
            print t
        
        o = self.bottom.forward(np.array([t]))
        return o

    def backprop(self,outputs,batch_size=1,dropout=0):
        o_deltas = self.bottom.backprop(outputs,batch_size=batch_size,dropout=dropout)
        r_deltas = np.dot(self.bottom.layers[0].forward_weights.T,o_deltas)
        if self.mean_pool:
            r_deltas /= self.input_length
            r_deltas = np.array([r_deltas for i in xrange(self.input_length)])
        else: 
            r_deltas = np.array([r_deltas])
        self.top.backprop(r_deltas,batch_size=batch_size,dropout=dropout)

    def update_weights(self,batch_size):
        for nn in [self.top,self.bottom]:
            nn.update_weights(batch_size)

    def activate_dropout(self):
        for nn in [self.top,self.bottom]:
            nn.activate_dropout()

    def deactivate_dropout(self):
        for nn in [self.top,self.bottom]:
            nn.deactivate_dropout()
    
    def dropout_scale(self):
        for nn in [self.bottom]:
            nn.dropout_scale()

    def dropout_rescale(self):
        for nn in [self.bottom]:
            nn.dropout_rescale()

    def inspect(self):
        for nn in [self.top,self.bottom]:
            nn.inspect()

class GRELU(Network):
    def __init__(self,size,gate_lrate=1,*args,**kwargs):
        super(GRELU,self).__init__(*args,**kwargs)
        self.size = size
        self.gate_lrate = gate_lrate
        self.relu = self.add_layer(Layer(
                parent_size=self.input_size,
                size=self.size,
                weight_factor=1,
                #eye=False,
                #activation=sigmoid,
                #activation_deriv=sigmoid_deriv,
                
                #weights=np.array([[1.0,0.0],[1.0,1.0]]),
            ))
        self.gates = self.add_layer(Layer(
                parent_size=self.input_size,
                size=self.size,
                activation=hsig,
                activation_deriv=hsig_deriv,
            ))

        self.layers = [self.relu,self.gates]
        #self.gates.forward_weights *= 0

        self.gates.biases = np.ones(self.gates.biases.shape) * 1.0
    def update_weights(self,*args,**kwargs):
        super(GRELU,self).update_weights(*args,**kwargs)
    def forward(self,inputs):
        self.inputs = inputs
        time = len(inputs)
        last_outputs = None

        for layer in self.layers:
            layer.inputs = np.zeros((time,layer.size))
            layer.activations = np.zeros((time,layer.size))

        self.output = []

        for t in range(time):
            input = inputs[t]
            if self.verbose:
                print "Input shape",inputs.shape,input,inputs
                print "time",t,input
            if not self.onehot_input:
                gates_z = np.dot(self.gates.forward_weights,input) + self.gates.biases
            else:
                gates_z = self.gates.forward_weights[:,input] + self.gates.biases
            
            self.gates.inputs[t,:] = gates_z
            
            gates_a = self.gates.activations[t,:] = self.gates.activation(gates_z)

            if self.verbose:
                print "Gates z",gates_z
                print "Gates a", gates_a

            if not self.onehot_input:
                relu_z = np.dot(self.relu.forward_weights,input) + self.relu.biases
            else:
                relu_z = self.relu.forward_weights[:,input] + self.relu.biases

            if self.verbose:
                print "Relu z, pre gates", relu_z

            if last_outputs != None:
                relu_z += np.dot(self.relu.recurrent_weights,last_outputs * gates_a)

            last_outputs = self.relu.activation(relu_z)
            self.relu.activations[t,:] = last_outputs.copy()
            self.relu.inputs[t,:] = relu_z
            self.output.append(last_outputs)

            if self.verbose:
                print "Relu z",relu_z
                print "Relu a",last_outputs

        return np.array(self.output)

    def backprop(self,y,batch_size=1,dropout=0):
        output_layer = self.layers[-1]
        if output_layer.size != y.shape[1]:
            raise Exception("Wrong output size for vector " + str(y))
        
        time_steps = self.inputs.shape[0]

        in_out_diff = max(0,time_steps - y.shape[0])

        time_range = range(in_out_diff,time_steps)[::-1]

        #forward_gradients = []
        #recurrent_gradients = []

        #for layer in self.layers:
        #    forward_times = [np.zeros(layer.forward_weight_updates.shape) for i in time_range]
        #    recurrent_times = [np.zeros(layer.recurrent_weight_updates.shape) for i in time_range]
        #    forward_gradients.append(forward_times)
        #    recurrent_gradients.append(recurrent_times)
        total_steps = 0
        _deltas = []
        for time in time_range:
            previous_deltas = None
            if self.verbose:
                print "outer time",time
                print "output, guess",y[time - in_out_diff],self.output[time]

            for bptt_step in range(max(0,time - self.cutoff),time+1)[::-1]:
                if bptt_step == time:
                    delta_out = self.cost_deriv(self.output[time],y[time - in_out_diff])
                    delta_relu = delta_out * self.relu.activation_deriv(self.relu.inputs[time])
                else:
                    #delta_relu = np.zeros(self.relu.size)
                    delta_relu = np.dot(self.relu.recurrent_weights,self.gates.activations[bptt_step + 1] * previous_deltas) * self.relu.activation_deriv(self.relu.inputs[bptt_step])
                    #np.dot(output_layer.recurrent_weights,previous_deltas[-1])

                inputs = self.inputs[bptt_step]
                self.relu.bias_updates += delta_relu / batch_size
                self.relu.forward_weight_updates += np.outer(delta_relu,inputs) / batch_size

                previous_deltas = delta_relu.copy()

                if bptt_step != 0:

                    self.relu.recurrent_weight_updates += np.outer(delta_relu,self.relu.activations[bptt_step - 1] * self.gates.activations[bptt_step]) /batch_size


                    #delta_gates = delta_relu * np.dot(self.relu.recurrent_weights,self.gates.activation_deriv(self.gates.inputs[bptt_step])) * self.relu.activations[bptt_step - 1]
                    delta_gates = delta_relu * np.dot(self.relu.recurrent_weights,self.relu.activations[bptt_step - 1]) * self.gates.activation_deriv(self.gates.inputs[bptt_step])
                    self.gates.bias_updates += delta_gates * self.gate_lrate / batch_size
                    self.gates.forward_weight_updates += np.outer(delta_gates,inputs) * self.gate_lrate / batch_size

                else:
                    _deltas.append(delta_relu)

        return _deltas



class Layer:
    def __init__(self,size,activation=relu,activation_deriv=relu_deriv,weight_factor=1,weights=None,recurrent=True,biased=True,parent_size=None,eye=True,do_dropout=True):
        self.size = size
        self.biases = np.zeros(size) #(np.random.rand(size) - 0.5) / np.sqrt(self.size)
        self.activation = activation
        self.activation_deriv = activation_deriv
        self.weight_factor = weight_factor
        self.forward_weights = weights
        self.recurrent = recurrent
        self.drop_forward = None
        self.drop_recurrent = None
        self.biased = biased
        self.parent_size = parent_size
        self.eye = eye
        self.do_dropout = do_dropout
    
    def __str__(self):
        return """
        Size:{}
        Activation:{}
        Activation deriv:{}
        Biased:{}
        Parent size:{}
        Identity weights:{}
        Weight factor for recurrent weights: {}
        Do dropout: {}
        """.format(
            self.size,
            self.activation,
            self.activation_deriv,
            self.biased,
            self.parent_size,
            self.eye,
            self.weight_factor,
            self.do_dropout
            )
    
    def init_weights(self):
        if not self.forward_weights != None:
            #                                                                         * 2   
            self.forward_weights = (np.random.rand(self.size,self.parent_size) -0.5) *2    / (np.sqrt(self.parent_size)) #* self.weight_factor
        
        self.forward_weight_updates = np.zeros((self.size,self.parent_size))
        self.forward_weight_updates_temp = np.zeros((self.size,self.parent_size))
        
        if self.eye:
            self.recurrent_weights = np.eye(self.size) * self.weight_factor#(np.random.rand(self.size,self.size) - 0.5) * 1/(np.sqrt(self.size))#* self.weight_factor
        else:
            self.recurrent_weights = (np.random.rand(self.size,self.size) -0.5) *2    / (np.sqrt(self.size)) * weight_factor
        
        self.recurrent_weight_updates = np.zeros(self.recurrent_weights.shape)
        self.recurrent_weight_updates_temp = np.zeros(self.recurrent_weights.shape)

        self.bias_updates = np.zeros(self.biases.shape)


        self.forward_lrates = np.zeros(self.forward_weights.shape)
        self.recurrent_lrates = np.zeros(self.recurrent_weights.shape)
        self.bias_lrates = np.zeros(self.biases.shape)

        #np.ones(self.weights.shape)

    def update_weights(self,batch_size,lrate,drate,clip):
        #print "max weight update",(self.weight_updates / batch_size).max()
        #self.forward_weights += -1 * lrate * self.forward_weight_updates / batch_size
        #self.recurrent_weights += -1 * lrate * self.recurrent_weight_updates / batch_size
        
        
        dx = np.maximum(-clip,np.minimum(self.forward_weight_updates,clip)) #/ batch_size
        """self.forward_lrates = np.maximum(-clip,np.minimum(clip,drate * self.forward_lrates - lrate * dx))
        self.forward_weights += self.forward_lrates
        self.forward_weights = np.maximum(-clip,np.minimum(clip,self.forward_weights))
        
        dx = self.recurrent_weight_updates

        self.recurrent_lrates = np.maximum(-clip,np.minimum(clip,drate * self.recurrent_lrates - lrate * dx))
        self.recurrent_weights += self.recurrent_lrates
        self.recurrent_weights = np.maximum(-clip,np.minimum(clip,self.recurrent_weights))
        dx = self.bias_updates 
        self.bias_lrates = np.maximum(-clip,np.minimum(clip,drate * self.bias_lrates - lrate * dx))
        self.biases += self.bias_lrates
        self.biases += np.maximum(-clip,np.minimum(clip,self.biases))

        #epsi = 10^(-3)
        """
        self.forward_lrates = np.maximum(-clip,np.minimum(clip,drate * self.forward_lrates + (1 - drate) * dx**2))
        self.forward_weights +=  np.maximum(-clip,np.minimum(clip, - lrate * (dx / (np.sqrt(self.forward_lrates) + epsi))))

        dx = np.maximum(-clip,np.minimum(clip,self.recurrent_weight_updates)) #/ batch_size

        self.recurrent_lrates = np.maximum(-clip,np.minimum(clip,drate * self.recurrent_lrates + (1 - drate) * dx**2))
        self.recurrent_weights +=  np.maximum(-clip,np.minimum(clip, - lrate * (dx / (np.sqrt(self.recurrent_lrates) + epsi))))

        if self.biased:
            dx = np.maximum(-clip,np.minimum(clip,self.bias_updates)) #/ batch_size

            self.bias_lrates = np.maximum(-clip,np.minimum(clip,drate * self.bias_lrates + (1 - drate) * dx**2))
            self.biases +=  np.maximum(-clip,np.minimum(clip, - lrate * (dx / (np.sqrt(self.bias_lrates) + epsi))))
        

        self.forward_weight_updates[:,:] = 0
        self.recurrent_weight_updates[:,:] = 0
        if self.biased:
            self.bias_updates[:] = 0
        

    def calc_weight_deltas(self,deltas,a):
        return np.outer(deltas,a)

    def inspect(self):
        print "forward lrates max,min,avg:",self.forward_lrates.max(),self.forward_lrates.min(),self.forward_lrates.mean(),"\n"
        print "recurrent lrates max,min,avg:",self.recurrent_lrates.max(),self.recurrent_lrates.min(),self.recurrent_lrates.mean(),"\n"
        print "bias lrates max,min,avg:",self.bias_lrates.max(),self.bias_lrates.min(),self.bias_lrates.mean(),"\n"        


        print "forward weights max,min,avg,zero",self.forward_weights.max(),self.forward_weights.min(),self.forward_weights.mean(),(np.isclose(0,self.forward_weights)).sum(),"\n"
        print "recurrent weights max,min,avg, diag avg,zero",self.recurrent_weights.max(),self.recurrent_weights.min(),self.recurrent_weights.mean(),self.recurrent_weights.diagonal().mean(),(np.isclose(0,self.recurrent_weights)).sum(),"\n"
        print "biases max,min,avg,zero",self.biases.max(),self.biases.min(),self.biases.mean(),(np.isclose(0,self.biases)).sum(),"\n"
        

        
        

