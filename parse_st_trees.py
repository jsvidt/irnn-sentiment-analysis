from ptb import *
import numpy as np

def to_str(self):
    if self.leaf():
        return self.leaf().word
    else:
        return '{}'.format(
            #self.head if self.head is not None else '',
            ' '.join(to_str(c) for c in self.children())
        )

def parse_sent(input):
    return parse(input)

def make_pre(stop=None,filter=lambda x: True,_round=False):
    def pre(t,state):
       # if (stop and len(state) >= stop):
        #    return state
        if state == -1:
            return state
        l = to_str(t) #+ "\t"

        if t.head.__class__ == Leaf:
            c = t.head.pos
        else:
            c = str(t.head)
        c = float(c) / 4
        
        #if (not filter(c)) and state == []:
        #    return -1
        #if _round: c = round(c)
        #l += str(c)
        state.append((c,l))
        return state
    return pre

def parse_tree(path,stop=None,filter=lambda x: True,_round=False):
    with file(path) as f:
        t = parse_sent(f.readlines())

        lines = []
        for l in t:
            state = []
            traverse(l,post=make_pre(stop,filter=filter,_round=_round),state=state)
            
            if stop:
                state = sorted(state,key=lambda x: len(x))[::-1][:stop]
                #print state
            for c,l in state:
                if filter(c,l):
                    if _round:
                        c = round(c)
                    l = l.replace("\/"," / ").replace("-"," ")
                    lines.append(l + "\t" + str(c))
        return lines

def load_additional():
    with file("sentiment/imdb_labelled.txt") as f:
        return f.readlines()

with file("processed.txt","w") as o:
    train = parse_tree("stanfordSentimentTreebank/trees/train.txt",3,filter=lambda x,y: x != .5 and len(y.split(" ")) > 0,_round=True)
    #train += load_additional()
    dev = parse_tree("stanfordSentimentTreebank/trees/dev.txt",1,filter=lambda x,y: x != .5,_round=True)
    test = parse_tree("stanfordSentimentTreebank/trees/test.txt",1,filter=lambda x,y: x != .5,_round=True)
    
    split = np.zeros(len(train) + len(dev) + len(test))
    split[0:len(train)] = 1
    split[len(train):len(train)+len(test)] = 2
    split[len(train)+len(test):len(train)+len(test)+len(dev)] = 3

    np.save("split.npy",split)
    lines = train + test + dev

    print (split == 1).sum(),len(train)
    print (split == 2).sum(),len(test)
    print (split == 3).sum(),len(dev)
    print split.shape,len(lines)

    

    for l in lines:
        o.write(l + "\n")

