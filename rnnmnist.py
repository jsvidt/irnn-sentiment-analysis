from rnn import *
def loadminst():
    imgs = np.load("imgs.txt.npy")[:500]
    labels = np.load("labels.txt.npy")[:500]
    outputs = imgs[:,:,np.newaxis]
    inputs = labels[:,np.newaxis,:]
    return outputs,inputs

inputs,outputs = loadminst()
print outputs.shape

rnn = Network(
            input_size=1,
            lrate=0.0001,
            drate=0.9,
            cost=cross_entropy,
            cost_deriv=log_likelihood_deriv,
            cutoff=800,
            #reg_beta=0.0001,
            #reg=l2norm,
            #reg_deriv=l2norm_deriv,
            clip_gradient=1,
            #decay_frequency=40,
            #decay_rate=0.3,
	    dropout=0.1,
        )


rnn.add_layer(Layer(
    size=10,
    #activation=sigmoid,
    #activation_deriv=sigmoid_deriv
    ))

#rnn.add_layer(Layer(
#    size=5,
    #activation=sigmoid,
    #activation_deriv=sigmoid_deriv
#    ))
#rnn.add_layer(Layer(
#    size=10,
    #activation=sigmoid,
    #activation_deriv=sigmoid_deriv
#    ))

#rnn.add_layer(Layer(
#    size=10,
#    activation=identity,
#    activation_deriv=identity_deriv,
#    ))

rnn.add_layer(Layer(
    size=outputs.shape[2],
    activation=softmax,
    activation_deriv=softmax_deriv,
    ))

def every(nn,outputs,guesses):
    guess_dist = np.zeros(nn.layers[-1].size)
    correct = 0
    #print guesses[:,:,-1]
    for i in xrange(len(guesses)):
        guess = guesses[i][-1]#nn.forward(inputs[i])[-1]
        
        #if k % 1000 == 0:
        #    print "guess", guess
        #    print "output", outputs[i]
        #print "max: ",np.argmax(guess)
        
        guess_dist[guess.ravel().argmax()] += 1
        correct += np.argmax(guess) == np.argmax(outputs[i])
        #print reconstruct_sentence(i,words,inputs),
        #print guess[-1]
    print "correct {}/{}".format(correct,len(guesses))
    print "guess dist {}".format(guess_dist)
    print "guess mean {}".format(guesses[0].mean(axis=0))
    print "true dist {}".format(outputs.sum(axis=0))


rnn.train(inputs,outputs,do_every=every,every=10,batch=10,iterations=100)
rnn.save("mnist.rnn")
