from rnn import *
from sentiment.loadsentences import *
from kfold import *
import sentiment.embeddings




words = np.load("sentence_words_dok.npy").item()

indexed_embeddings = np.load("indexed_embeddings.npy")


def createnn():
    #rnn = Network.load("rotten_validation/0_nn.npy")
    #rnn.reg_beta = 0
    #rnn.reg_beta_r = 0
    #rnn.reg_beta_b = 0
    #rnn.lrate = 0.00001
    #rnn.clip_gradient = 100
    #return rnn
   
    urnn = Network(
            input_size=len(words),
            lrate=0.001,
            drate=0.99,
            cost=log_likelihood,
            cost_deriv=log_likelihood_deriv_for_softmax,#log_likelihood_deriv,
            cutoff=4000,
            #reg_beta=0.001,#.000001,
            #reg_beta_r=0.001,#.000001,
            #reg_beta_b=0.001,
            #reg=l2norm,
            #reg_deriv=l2norm_deriv,
            clip_gradient=100,
            onehot_input=True,
            #decay_frequency=5000,
            #decay_rate=0.05,
            dropout=0.5,
            )
    urnn.add_layer(Layer(
        size=300,
        activation=identity,
        activation_deriv=identity_deriv,
        weights=indexed_embeddings,
        recurrent=False,
        biased=False,
        do_dropout=False,
        #weight_factor=0.75,
        #biased=False,
        ))
    urnn.add_layer(Layer(
        size=300,
        #activation=hsig,
        #activation_deriv=hsig_deriv
        #weight_factor=0.75,
        #biased=False,
        ))
    urnn.add_layer(Layer(
        size=300,
        #activation=hsig,
        #activation_deriv=hsig_deriv
        #weight_factor=0.75,
        #biased=False,
        ))
    urnn.add_layer(Layer(
        size=2,
        activation=softmax,
        activation_deriv=softmax_deriv,
        recurrent=False,
        biased=True,
        ))
    urnn.inspect()

    return urnn

nns = []

best = {
    "training":0,
    "validation":0,
    "final": 0,
}


import copy
from ensemble import *
def every(nn,outputs,guesses,label,inputs,ensemble):
    guess_dist = np.zeros(nn.layers[-1].size)
    correct = 0
    esum = 0
    #print guesses[:,:,-1]
    #print "Guesses shape",guesses.shape
    elen = len(ensemble.community)
    if label == "training":
        nn.inspect()
    for i in xrange(guesses.shape[0]):
        guess = guesses[i][-1]
        #print "Shape",guess.shape
        
        #if k % 1000 == 0:
        #    print "guess", guess
        #    print "output", outputs[i]
        #print "max: ",np.argmax(guess)
        
        guess_dist[guess.argmax()] += 1
        correct += np.argmax(guess) == np.argmax(outputs[i])
        #print reconstruct_sentence(i,words,inputs),
        #print guess[-1]
        #if i % 100 == 0:
        #    print "correct, so far {}/{}".format(correct,i)
        if elen > 0 and label == "final":
            eguess = ensemble.forward(inputs[i],"{}-{}".format(label,i))
            ecorrect = np.argmax(eguess) == np.argmax(outputs[i])
            #if not ecorrect:
            #    print "ENSEMBLE",eguess,outputs[i]
            esum += ecorrect
    percentage = correct/float(len(guesses)) * 100
    #if percentage > 70 and label == "validation":
        #nn.clip_gradient = 0.1
    #    nn.lrate = 0.0001
    #    print "LRATE NOW",nn.lrate
        #print "CLIPPING AT ", nn.clip_gradient
    ep = esum / float(len(guesses)) * 100
    if best[label] <= percentage:
        if label == "validation":
            nn.best_validation_iteration = nn.i
        best[label] = percentage
    if label == "validation":
        nn.certainty = percentage
        #if percentage > 80:
        #    nn.every = 1
        #else:
        #    nn.every = 50
        #    nn.reg_beta=0.0000001
        #    nn.reg_beta_r=0.0000001
        #    nn.reg=l2norm
        #    nn.reg_deriv=l2norm_deriv
        print "reg_beta",nn.reg_beta
        #ensemble.add_member(copy.deepcopy(nn))
    if label == "final" and nn.best_validation_iteration == nn.i:
        with open("results.txt","w") as f:
             f.write("""
Results
     Iteration: {}
     Training: {} %
     Validation: {} %
     Final at i: {} %
     Final overall: {}%
     Final ensemble: {}%
             """.format(i,best["training"],best["validation"],percentage,best["final"],ep))
             f.write(str(nn))
    print "correct {}/{}, {}%".format(correct,len(guesses),percentage),"max: ", best[label]
    print "guess dist {}".format(guess_dist)
    print "guess mean {}".format(guesses[0].mean(axis=0))
    print "true dist {}".format(outputs.sum(axis=0))
    print "epercentage", ep

"""

def every(nn,_outputs,guesses):
    guess_dist = np.zeros(nn.layers[-1].size)
    correct = 0
    #print guesses[:,:,-1]
    for i in xrange(len(outputs)):
        guess = nn.forward(inputs[i])[-1]
        
        #if k % 1000 == 0:
        #    print "guess", guess
        #    print "output", outputs[i]
        #print "max: ",np.argmax(guess)
        
        guess_dist[guess.ravel().argmax()] += 1
        correct += np.argmax(guess) == np.argmax(outputs[i])
        #print reconstruct_sentence(i,words,inputs),
        #print guess[-1]
    print "correct {}/{}".format(correct,len(outputs))
    print "guess dist {}".format(guess_dist)
    print "guess mean {}".format(guesses[0].mean(axis=0))
    print "true dist {}".format(outputs.sum(axis=0))
"""
#rnn = createnn()
#rnn.train(np.array(inputs),np.array(outputs),do_every=every,every=30,batch=50,iterations=1000)
#rnn.save("sentiment.rnn")
split = np.load("split.npy")
print "num 4",(split == 4).sum(), "num 1", (split == 1).sum()
#split[split == 4] = 1
print "num 1 after",(split == 1).sum()

kFoldCross(createnn,
    "sentence_inputs_dok.npy",
    "sentence_outputs_dok.npy",
    every,
    folds=10,
    iterations=100000,
    every=50,
    batch=20,
    super_iterations=1,
    save_in="rotten_validation"
    ,kf=[(split==1,split==3,split==2)],
    #flip=True,
    )
