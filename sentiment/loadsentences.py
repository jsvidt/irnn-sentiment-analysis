import numpy as np
import nltk
from nltk.tokenize import TweetTokenizer
from collections import OrderedDict
import codecs
from scipy.sparse import csr_matrix, lil_matrix
import sklearn
from embeddings import *
from nltk import word_tokenize
import time
labels = {
    "negative": 0,
    "positive": 1,
    "objspam": 2,
    "objnspam": 2,
    }
def clstoindex(label):
    return labels[label]

def clstoindex_stanford(label):
    label = float(label)
    cls = [0,0]
    #if label > .5:
    cls[0] = 1 - label
    cls[1] = label
    #else:
    #    cls[0] = label
    #    cls[1] = 1 - label
    return cls


tok = TweetTokenizer(strip_handles=False,reduce_len=True)
#def loadsentences(path="twitter-data/Manually-Annotated-Tweets.tsv",number=None,label_index = -1,sentence_index=0,delimiter="\t",min_frequency=0,classes=3,class_to_index=clstoindex,use_embeddings=False):
#def loadsentences(path="rotten_imdb/processed.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=None,use_embeddings=False):
#def loadsentences(path="rt-polaritydata/processed.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=None,use_embeddings=False):
#def loadsentences(path="review_polarity/processed.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=None,use_embeddings=False):
#def loadsentences(path="sentiment/imdb_labelled.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=None,use_embeddings=False,reverse=False):
#def loadsentences(path="UMICH.SI650/training.txt",number=None,label_index = 0,sentence_index=1,delimiter="\t",min_frequency=0,class_to_index=None,classes=2):
#def loadsentences(path="sentiment/Sentiment Analysis Dataset.csv",number=None,label_index = 1,sentence_index=3,delimiter=",",min_frequency=0,class_to_index=None,classes=2):
#def loadsentences(path="processed.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=None,use_embeddings=False):
def loadsentences(path="processed.txt",number=None,label_index = 1,sentence_index=0,delimiter="\t",min_frequency=0,classes=2,class_to_index=clstoindex_stanford,use_embeddings=False,reverse=False):
    split = np.load("split.npy")
    sentences = []
    outputs = []
    words = set()
    i = 0
    last_l = []
    sent_set =  set()
    num_dupes = 0
    len_dupes = 0
    total = 0
    with open(path,"r") as f:
        for l in f.readlines():
            #l = codecs.decode(l,"UTF-8")
            if number != None and i > number:
                break
            
            l = l.strip().split(delimiter)
            #print l

            #if last_l:
            #    l = last_l + l
            #    last_l = []
            if len(l) < 2:
                continue
            try:
                raw_c = l[label_index].strip()
                if not class_to_index:
                    _out = int(raw_c)
                    real_out = [0 for x in xrange(classes)]
                    real_out[_out] = 1
                else:
                    real_out = class_to_index(raw_c)

            except ValueError,KeyError:
                #last_l = l
                #print last_l
                print "LABEL",l[label_index]
                continue

            if sentence_index < label_index or label_index < 0:
                sent = l[sentence_index:label_index]
            else:
                sent = l[sentence_index:]
            _in = " ".join(sent).strip().lower()
            total += 1
            #if " ".join(sent) in sent_set:
            #    num_dupes += 1
            #    len_dupes += len(sent)
            #    split[total - 1] = 0
                #continue

            sent_set.add(" ".join(sent))
            sent = word_tokenize(_in.decode("utf-8"))
            if reverse:
                sent = sent[::-1]
            sentences.append(sent)

            

            outputs.append([real_out])
            i += 1
    j = 0
    #np.save("split.npy",split)
    """
    new_sents = []
    for sent in sentences:
        if not sent:
            continue
        new = []
        for i,w in enumerate(sent):
            if not w:
                "NOT W"
            neww = w
            if w[0] == "'" and len(w) > 1:
                neww = w[1:]
            if "-" in w:
                s = w.split("-")
                for subs in s:
                    if subs:
                        new.append(subs)
                neww = None
            if "/" in w:
                s = w.split("/")
                for subs in s:
                    if subs:
                        new.append(subs)
                neww = None
            if neww:
                new.append(neww)
        new_sents.append(new)
        print j
        j += 1

    sentences = new_sents
    """
    for sent in sentences:
        for w in sent:
            words.add(w)

    print "Number of duplicates: ", num_dupes
    #print "Avg len of dupes:", len_dupes / float(num_dupes)
    words = OrderedDict([(v,k) for k,v in enumerate(list(words))])
    dist = dict([(w,0) for w in list(words.iterkeys())])

    #words_to_embeddings(words)
    #return

    for sent in sentences:
        for w in sent:
            if not w:
                print "EMPTY word"
                continue
            dist[w] += 1

    top = set([w for w,c in sorted(dist.iteritems(),key=lambda x: x[1])[::-1][:10]])
    print "Word distribution, First 10", 
    below_min_frequency = set([w for w,c in dist.iteritems() if c < (min_frequency + 1)])
    print "Infrequent", below_min_frequency

    print "# words, before remove,", len(words)
    #remove = set(["-","?",",",".","..."]) | top | below_min_frequency
    remove = below_min_frequency#set()
    print "Removing words,", remove
    words = list(set(words.iterkeys()) - remove)#[v for v in words.iterkeys() if v not in remove]

    print "# words, after removing,", len(words)
    words = OrderedDict([(v,k) for k,v in enumerate(words)])
    indexed_embeddings = words_to_indexed_embeddings(words)
    if use_embeddings:
        embeddings = words_to_embeddings(words)
        inputs = make_sentence_vectors_embedded(sentences,embeddings,outputs)
    else:
        embeddings = None
        inputs = make_sentence_vectors(sentences,words,outputs)

    #print "Sumstuff", inputs.sum(axis=0) == True
    #print "All zero sents: ",(np.array(inputs.sum(axis=0) == True)).sum()

    return words,inputs,np.array(outputs),embeddings, dist,indexed_embeddings

    
    #print len(dist)

    #return np.array(inputs),np.array(outputs)

def make_sentence_vectors(sentences,words,outputs):
    inputs = []
    i = 0
    no_words = 0
    infreq = 0
    
    for sent in sentences:
        _in = [None for w in sent if words.has_key(w)] * 1#lil_matrix((len([w for w in sent if words.has_key(w)]),len(words)),dtype=np.float)
        j = 0
        for k,w in enumerate(sent):
            if w == "n't":
                w = "not"
            if words.has_key(w):
                make_word_vector(w,words,_in,j)
                j+=1
            else:
                infreq += 1

        if j == 0:
            del outputs[i]
            print "NO WORDS IN SENT"
            no_words += 1

        else:
            inputs.append(np.array(_in))
        if i % 100 == 0:
            print "Sent:", i
        i+=1
    print "# of sents with no words:", no_words
    print "# of infreq", infreq
    return inputs

def make_sentence_vectors_embedded(sentences,words,outputs):
    inputs = []
    i = 0
    no_words = 0

    for sent in sentences:
        _in = np.zeros((len([w for w in sent if words.has_key(w)]),300),dtype=np.float)#lil_matrix((len([w for w in sent if words.has_key(w)]),len(words)),dtype=np.float)
        j = 0
        for w in sent:
            if words.has_key(w):
                #make_word_vector(w,words,_in,j)
                _in[j,:] = words[w]
                j+=1
            else:
                try:
                    print "no has no embedding", w
                except Exception:
                    pass
        if j == 0:
            del outputs[i]
            print "NO WORDS IN SENT"
            no_words += 1
        else:
            inputs.append(_in)
        if i % 100 == 0:
            print "Sent:", i
        i+=1
    print "# of sents with no words:", no_words
    return inputs


def make_word_vector(w,words,_in,index):
    #_in = csr_matrix((len(words),))#np.zeros(len(words))
    #_in[index,words[w]] = 1
    
    _in[index] = words[w]

def reconstruct_sentence(index,words,inputs,embeddings=None):
    #print inputs[index]
    if not embeddings:
        out = []
         
        for w in inputs[index]:#[words.keys()[w.toarray().argmax()] for w in inputs[index]]
            if w:
                out.append(words.keys()[int(w)])
            else:
                out.append("NONE")
        return out
    else:
        rev_embeddings = dict([(" ".join(v),k) for k,v in embeddings.iteritems()])
        sent = []
        for w in inputs[index]:
            sent.append(rev_embeddings[" ".join([str(w_1) for w_1 in w])])
        return sent
        #return [rev_embeddings["".join(str(w))] for w in inputs[index]]


#inputs,outputs = 
if __name__ == "__main__":
    words,inputs,outputs,embeddings,dist,indexed_embeddings = loadsentences()
    #inputs,outputs = sklearn.utils.shuffle(inputs,outputs,random_state=10)
    #inputs,outputs = inputs[:200],outputs[:200]
    np.save("sentence_words_dok.npy",words)
    np.save("sentence_inputs_dok.npy",inputs)
    np.save("sentence_outputs_dok.npy",outputs)
    np.save("indexed_embeddings.npy",indexed_embeddings)
    np.save("embeddings.npy",embeddings)
    np.save("dist.npy",dist)
    print "Inputs: ", len(inputs), inputs[0].shape
    print "Outputs: ", outputs.shape
    print "Words: ", len(words)
    print "Output dist:",outputs.sum(axis=0)

    for i in xrange(len(inputs)):
        rev_labels = dict([(v,k) for k,v in labels.iteritems()])
        print outputs[i],rev_labels[outputs[i].argmax()],reconstruct_sentence(i,words,inputs,embeddings)
