import codecs
import numpy as np
from loadsentences import *

def words_to_embeddings(words,file="glove.6B.100d.txt"):
    """
        Words should be a dict of words with key as textual repr and id value
        returns dict with word as key and value as word embedding
    """
    if words:
        wset = set(words.iterkeys())
    else:
        wset = set()
    #embeddings = dict()
    words = []
    embeddings = []
    i = 0
    with codecs.open(file) as f:
        for l in f.readlines():
            l = l.strip().split(" ")
            w = l[0]
            e = [float(e.strip()) for e in l[1:]]
            if w in wset or len(wset) == 0:
                words.append(w)
                embeddings.append(e)
                #embeddings[w] = e
            if i % 10000 == 0: print i
            i += 1


    #if len(embeddings.keys()) != len(embeddings.keys()):
    #    print "Not all words in glove"
    
    return dict(zip(words,embeddings))


def words_to_indexed_embeddings(words,file="glove.6B.300d.txt"):
    """
        Words should be a dict of words with key as textual repr and id value
        returns dict with word as key and value as word embedding
    """
    
    wset = set(words.iterkeys())
    #embeddings = dict()
    #words = []
    embeddings = np.zeros((300,len(words),))

    i = 0
    with codecs.open(file) as f:
        for l in f.readlines():
            l = l.strip().split(" ")
            w = l[0]
            e = [float(e.strip()) for e in l[1:]]
            if w in wset or len(wset) == 0:
                embeddings[:,words[w]] = e
                #embeddings[w] = e
            if i % 10000 == 0: print i
            i += 1

    #if len(embeddings.keys()) != len(embeddings.keys()):
    #    print "Not all words in glove"
    
    return embeddings



def load_and_save(words=[],output="glove.npy"):
    e = words_to_embeddings(words)
    np.save(output,e)

def concat_embedding(e):
    return " ".join([str(w) for w in e])

def nn(words,_all=None,file="glove.6B.100d.txt",k=5):
    """
        Words should be a dict of words with key as textual repr and id value
        returns dict with word as key and value as word embedding
    """
    words = words_to_embeddings(dict([(w,0) for w in words]))
    embs = np.array(words.values())
    def dist(e,o):
        return (np.sqrt(((e - o)**2).sum(axis=1))).argsort()[1:k+1]
    
    all = words_to_embeddings(_all)
    all_words = np.array(all.keys())
    
    all_embs = np.array(all.values())
    neighbours = dict()
    for w,e in words.items():
        dists = dist(all_embs,e)
        neighbours[w] = all_words[dists]
        print w,dists,neighbours[w] 

    """
    words = dict([(i[0],[float(d) for d in i[1]]) for i in words.iteritems()])
    neighbours = dict([(w,[]) for w in words.keys()])
    def update_neigbours(w,ow,oe):
        d = dist(words[w],oe)
        neighbours[w].append((ow,oe,d))
        neighbours[w] = sorted(neighbours[w],key=lambda x: x[2])[:1]
    
    i = 0    
    with codecs.open(file) as f:
        for l in f.readlines():
            l = l.strip().split(" ")
            w = l[0]
            e = np.array([float(e.strip()) for e in l[1:]])
            for word in words.keys():
                if w != word:
                    update_neigbours(word,w,e)
            if i % 10000 == 0: print i
            i += 1    
    """
    return neighbours

def reverse_embeddings(embeddings):
    rev_embeddings = dict([(" ".join(v),k) for k,v in embeddings.iteritems()])
    return rev_embeddings

def augment(inputs,outputs,words,dist,split,min_freq=1,k=1):
    extra = []
    extra_outputs = []
    rev = dict([(i,w) for w,i in words.iteritems() ])
    infrequent = [w for w,i in  dist.iteritems() if i < min_freq + 1]
    all_inputs = inputs.copy()
    inputs = inputs[split == 1]
    nns = nn(infrequent,_all=words,k=k)
    for i in xrange(inputs.shape[0]):
        replacements = dict()
        for e in inputs[i]:
            w = rev[e]
            if dist[w] < min_freq + 1:
                print "INFREQUENT: ", w
                try:
                    for ow in nns[w]:
                        replacements[e] = words[ow]
                        #print replacements
                        #print "SUGGESTION: ", ow
                except KeyError:
                    pass
                    #print w,
            #else: print w,
        for e,oe in replacements.items():
            cpy = inputs[i].copy()
            cpy[cpy == e] = oe
            extra.append(cpy)
            extra_outputs.append(outputs[i])
        #print "\n"

    inputs = np.concatenate((all_inputs,extra),axis=0)
    outputs = np.concatenate((outputs,extra_outputs),axis=0)
    extra_split = np.ones(len(extra)) * 4
    split = np.concatenate((split,extra_split),axis=0)
    print len(extra),len(extra_outputs),len(extra_split)
    print len(inputs),len(outputs),len(split)
    np.save("sentence_inputs_dok.npy",inputs)
    np.save("sentence_outputs_dok.npy",outputs)
    np.save("split.npy",split)
    #for i in xrange(len(extra)):
    #    print "extra",i
    #    print reconstruct_sentence(i,words,extra,embeddings=None)


if __name__ == "__main__":
   #embs = words_to_embeddings(None)
   augment(np.load("sentence_inputs_dok.npy"),np.load("sentence_outputs_dok.npy"),np.load("sentence_words_dok.npy").item(),np.load("dist.npy").item(),np.load("split.npy"))
