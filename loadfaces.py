import sklearn

import numpy as np
from PIL import Image,ImageOps
import os

#emotions = {
#    "neutral":,
#    "sad":,
#    "":,
#}

def walk(path,R,labels,num,i=0,ending=".jpg",size=(50,50)):
    done = False
    for root, dirs,files in os.walk(path):
        if done:
            break
        for file in files:
            if file[-4:] in (ending,ending.upper()):
                fp = os.path.join(root,file)
                file = file.lower()
                im = Image.open(fp).convert("L")
                im = ImageOps.fit(im,(50,50),Image.ANTIALIAS)
                


                R[i,:] = np.array(im.getdata()).flatten() / 255.0
                if "female" in file:
                    labels[i,1] = 1
                elif "male" in file:
                    labels[i,0] = 1
                else:
                    print "NO LABEL!!"
                i += 1
            if i > num:
                done = True
                break
        for dir in dirs:
            dp = os.path.join(root,dir)
            if i > num:
                done = True
                break
            i = walk(dp,R,labels,num,i)
    return i

def savefaces(path,num_labels=2,num=528,size=(50,50)):
    R = np.zeros((num,size[0] * size[1]))
    labels = np.zeros((num,num_labels))
    walk(path,R,labels,num)
    labels = labels[R.sum(axis=1) > 0]
    R = R[R.sum(axis=1) > 0]
    print "labelmean:",labels.sum(axis=1).mean()
    print "faces:",R.shape,"labels:",labels.shape
    print "empties:", (R.sum(axis=1) == 0).any()
    R,labels = sklearn.utils.shuffle(R,labels,random_state=10)
    np.save("faces",R)
    np.save("face_mf_labels",labels)

if __name__ == "__main__":
    savefaces("faces")