import codecs

with codecs.open("processed.txt","w","utf-8") as o:
    for label,pol in enumerate(["neg","pos"]):
        with codecs.open("rt-polaritydata/rt-polarity." + pol) as f:
            for l in f.readlines():
                l = l.strip().decode("latin-1")
                o.write(l + "\t" + str(label) + "\n")
