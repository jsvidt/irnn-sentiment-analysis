import rnn
from sentiment.loadsentences import *
from rnn import *
import numpy as np
from matplotlib import pyplot as plt


def plt_words(nn,words):
    #plt.ion()
    
    #out = plt.imshow(imgs[0].reshape(size))
    #plt.plot()
    text1 = plt.text(-0.5,-.6,"hejsa",color="white",size="large")
    word = make_word_vector(words.keys()[0],words)[np.newaxis,:]
    guess = nn.forward(word)
    guess_image = plt.imshow(guess[-1][np.newaxis,:])
    #text2 = plt.text(-0.5,.6,"hejsa",color="white",size="large")
    #plt.show()
    for i in xrange(len(words)):
        #time.sleep(20)
        for prepend in ["","not","very","good","great","bad"]:
            word = make_word_vector(words.keys()[i],words)
            if prepend:
                try:
                    prepend_word = make_word_vector(prepend,words)
                except KeyError:
                    raise Exception("Couldn't find word: ",prepend)
                _input = np.array([prepend_word,word])
            else:
                _input = np.array([word])
                

            guess = nn.forward(_input)
            #_input.set_data(imgs[i].reshape(size))
            
            #val = u" ".join(sent)#join(["{}: {}".format(w,guess[i]) for i,w in enumerate(sent)])
            text1.set_text(reconstruct_sentence(0,words,np.array([_input])))
            
            guess_image.set_data(guess[-1][np.newaxis,:])#nn.layers[-1].activations[np.newaxis,:])
            #text2.set_text(sent[j])
               
            plt.draw()
            plt.pause(2)
    

#imgs = np.load("imgs.txt.npy")
#labels = np.load("labels.txt.npy")
#inputs = np.load("mnist_validation/0_input_test.npy")
#labels = np.load("mnist_validation/0_output_test.npy")
#words,inputs,outputs = loadsentences()
#words = 
nn = np.load("rotten_validation/0_nn.npy").item()

plt_words(nn,words)