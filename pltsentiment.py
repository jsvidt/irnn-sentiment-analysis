import rnn
from sentiment.loadsentences import *
from rnn import *
import numpy as np
from matplotlib import pyplot as plt
from sklearn.utils import shuffle

def plt_sentiment(nn,words,inputs,outputs):
    #plt.ion()
    
    #out = plt.imshow(imgs[0].reshape(size))
    #plt.plot()
    
    grid = (len(nn.layers),1)
    for li,layer in enumerate(nn.layers):
        plt.subplot2grid(grid,(li,0))
        layer.plot = plt.imshow(layer.activations[0][np.newaxis,:])
    text1 = plt.text(-0.5,0.2,"hejsa",color="white",size="large")
    text2 = plt.text(-0.5,0.5,"hejsa",color="white",size="large")    
    #plt.show()
    for i in xrange(len(inputs)):
        #time.sleep(20)
        guess = nn.forward(inputs[i])
        #_input.set_data(imgs[i].reshape(size))
        sent = reconstruct_sentence(i,words,inputs)
        try:
            val = " ".join(sent)#join(["{}: {}".format(w,guess[i]) for i,w in enumerate(sent)])
            text1.set_text("guess: {}, actual: {}".format(guess[-1].argmax(),outputs[i].argmax()) + "\n" + val)
        except UnicodeDecodeError:
            text1.set_text("Unicode error")
        for j in xrange(inputs[i].shape[0]):
            for layer in nn.layers:
                layer.plot.set_data(layer.activations[j][np.newaxis,:])#nn.layers[-1].activations[np.newaxis,:])
            try:
                text2.set_text(sent[j])
            except UnicodeDecodeError:
                text2.set_text("Unicode error")
            plt.draw()
            plt.pause(0.2)
        

#imgs = np.load("imgs.txt.npy")
#labels = np.load("labels.txt.npy")
#inputs = np.load("mnist_validation/0_input_test.npy")
#labels = np.load("mnist_validation/0_output_test.npy")
#words,inputs,outputs = loadsentences()
words = np.load("sentence_words_dok.npy").item()

inputs = np.load("rotten_validation/0_input_test.npy")
outputs = np.load("rotten_validation/0_output_test.npy")

#inputs,outputs = shuffle(inputs,outputs)

#for i in xrange(500):
#    rev_labels = dict([(v,k) for k,v in labels.iteritems()])
#    print rev_labels[outputs[i].argmax()],reconstruct_sentence(i,words,inputs)

#inputs = inputs[:20]
#inputs = outputs[:20]


#sents = [
#    ["this","was","crap"],
#    ["worst","movie","ever"],
#    ["the","movie","was","very","bad",],
#    ["this","movie","could","have","been","very","bad","had","it","not","been","for","great","actors"],
#]

#inputs = []


#inputs = np.array(make_sentence_vectors(sents,words))
nn = np.load("rotten_validation/0_nn.npy").item()

plt_sentiment(nn,words,inputs,outputs)