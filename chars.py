import idx2numpy
import numpy as np

def loadchars(path="chars/train-images-idx3-ubyte",label_path="chars/train-labels-idx1-ubyte",num=20000,max_label=10):
    labels = idx2numpy.convert_from_file(label_path)
    mat = idx2numpy.convert_from_file(path)
    #R = np.zeros((num,28*28,))
    #i = 0
    #c = 0

    #actual_labels = []
    #
    #while i < num:
    #    if -1 < labels[c] < 5:
    #        R[i,:] = mat[c].flatten() / 255.0
    #        actual_labels.append(labels[c])
    #        i += 1
    #    c += 1
    #print "all filled", (R == np.nan).any() == False
    
    #actual_labels = np.array(actual_labels)
    #print actual_labels
    R = mat[labels < max_label,:,:].reshape((labels < max_label).sum(),mat.shape[1] * mat.shape[2])
    actual_labels = labels[labels < max_label]
    
    
    ones = np.zeros((actual_labels.size,actual_labels.max() + 1))
    ones[np.arange(actual_labels.size),actual_labels] = 1
    
    print R.shape,actual_labels.shape
    
    R = R[:num]
    ones = ones[:num]
    print R.shape,ones.shape
    return R,ones
    
imgs,labels = loadchars()

np.save("imgs.txt",imgs)
np.save("labels.txt",labels)