import numpy as np

epsi = np.finfo(float).eps
fmax = np.finfo(float).max
import numexpr as ne

###
#       activation functions
###
def identity(z):
    return z
    
def identity_deriv(z):
    return 1

def relu(z):
    sum = z.sum()
    return np.maximum(z,0) #/ sum

def relu_deriv(z):
    return ne.evaluate("(z > 0) * 1")#ne.evaluate("(z > 0) * 1") #+ (z <= 0) * 0.000001 #np.sign(z) + ((z == 0) * 1)


def sigmoid(z):
    #print -1 * z
    a = -1 * z
    b = np.exp(a)
    return 1 / (1 + np.nan_to_num(b))

def sigmoid_deriv(z):
    return sigmoid(z) * (1 - sigmoid(z))

def hsig(z):
    return np.minimum(np.maximum(z,0),1)

def hsig_deriv(z):
    return ne.evaluate("((z > 0) & (z < 1)) * 1")

def sigmoid_deriv_for_cross_entropy(z):
    return 1

def cross_entropy_softmax(a,y):
    return -y * a - np.log(np.sum(np.exp(a),axis=0))
    
def cross_entropy_softmax_deriv(a,y):
    return a - y

#cross'()*soft'()

#cross(soft(sdfasdf))

def softmax(z):
    """
        Safe softmax, where both numerator and denominator is scaled by e^max(k), preventing overflow
        Note: e^x*e^y = e^(x+y)
    """
    m = np.max(z)
    #_z = np.nan_to_num(z - m)
    _z = ne.evaluate("z - m")#
    #return np.exp(_z)/np.sum(np.exp(_z),axis=0)
    denom = ne.evaluate("sum(exp(_z),axis=0)")
    return ne.evaluate("exp(_z)/denom")

def softmax_deriv(x):
    return 1#np.diag(x) - np.outer(x,x)



###
#   regularizers
###


def l2norm(w):
    return ne.evaluate("(w**2)/2")

def l2norm_deriv(w):
    return ne.evaluate("w")

###
#       Cost functions
###

def squared_error(a,y):
    return (a - y) ** 2

def squared_error_deriv(a,y):
    return (a - y) * 2
    
    
def cross_entropy(a,y):
    """
        Categorical cross entropy
        -y * np.log(a + epsi)
        Binomial
        -y * np.log(a + epsi) - (1-y) * np.log(1 - a + epsi)
        Url: https://www.reddit.com/r/MachineLearning/comments/39bo7k/can_softmax_be_used_with_cross_entropy/
    """
    return -y * np.log(a + epsi) - (1-y) * np.log(1 - a + epsi) #ne.evaluate("-y * log(a + epsi) - (1-y) * log(1 - a + epsi)")
    
#def cross_entropy_deriv(a,y):
#    return -y / (a + epsi) - (1 - y) / (1 - a + epsi)

def cross_entropy_deriv(a,y):
    """
        Categorical cross entropy deriv
        -y / (a + epsi)
        Binomial deriv
        (a-y) / (a*(1 - a) + epsi)
        Url: https://www.reddit.com/r/MachineLearning/comments/39bo7k/can_softmax_be_used_with_cross_entropy/
    """

    return (a-y) / (a*(1 - a) + epsi)

def cross_entropy_deriv_for_sigmoid(a,y):
    return (a-y)
    
def log_likelihood(a,y):
    """
        Categorical cross entropy = Log likelihood
        Url: https://www.reddit.com/r/MachineLearning/comments/39bo7k/can_softmax_be_used_with_cross_entropy/
    """
    return  -y * np.log(a + epsi)#-np.nan_to_num(np.log(a.dot(y)))# 

def log_likelihood_deriv(a,y):
    return -y / (a + epsi)
    #return a - y#ne.evaluate("a - y")

def log_likelihood_deriv_for_softmax(a,y):
    return (a - y)


def connect(a,y):
    return 1

def connect_deriv(a,y):
    return y
